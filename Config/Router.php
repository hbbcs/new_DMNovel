<?php namespace Config;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-11-5
 * Time: 下午3:45
 */

$config['router'] = [
    '/' => 'Index@index',
    '/user' => 'User@index',
    '/book/chapters/:a/:a/:a' => 'Book@chapters',
    '/book/:a' => 'Book@index',
    '/hot/:n/:n' => 'Index@getHots',
    '/category/:n' => 'Category@index',
    '/read/:a/:a' => 'Book@read',
    '/download/:sn/:sn' => 'Download@index',
    '/update/:a' => 'Update@index',
    '/notice/:n' => 'Notice@index',
];