<?php namespace Config;

$config['title'] = '東木书屋';
$config['baseUrl'] = "http://" . $_SERVER['HTTP_HOST'] . "/DMNovel";
$config['language'] = 'zh_CN';
/**
 * 设置缓存需要的信息
 *
 * name     缓存名称
 * path     缓存存放目录
 * extension    缓存扩展名
 */
$config['cache'] = [
    'cache_name' => 'ce',
    'cache_path' => BASE_PATH . '/assets/cache/',
    'cache_extension' => '.php',
    'cache_expired' => '7200',
];

$config['theme'] = "bootstrap/";

$config['view'] = [
    'path' => BASE_PATH . '/App/Views/' . $config['theme'] ?: "default" . '/',
    'expired' => '0',
];

/**
 * 默认调用DB库
 */
$config['db'] = true;

$config['development'] = false; //设置开发模式,true 显示全部信息,false 只显示标题