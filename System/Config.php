<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-11-5
 * Time: 下午3:17
 */

namespace System;


class Config {

    private $config=[];

    public function __construct($filename='') {
        $this->load('Config');
        if ($filename) {
            $this->load($filename);
        }
    }

    public function get($name) {
        return $this->config[$name] ?: '';
    }

    public function set($name, $value) {
        $this->config[$name] = $value;
    }

    public function __get($name) {
        return $this->config[$name] ?: '';
    }

    public function __set($name, $value) {
        $this->config[$name] = $value;
    }

    public function load($filename) {
        if (file_exists(BASE_PATH.'/Config/'.$filename.'.php')) {
            $config=[];
            require BASE_PATH.'/Config/'.$filename.'.php';
            $this->config=array_merge($this->config,$config);
        } else {
            showError('Open Config File Error (/Config/'.$filename.'.php)');
        }
    }
}