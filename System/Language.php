<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-10-31
 * Time: 下午3:01
 */

namespace System;


class Language {
    private $language     = [];
    private $path         = BASE_PATH . '/App/Language/';
    private $default_file = ['system/view'];

    function __construct($code = 'zh_CN', $file = '') {
        $this->path = $this->path . $code;
        array_push($this->default_file,$file);
        foreach ($this->default_file as $default) {
            $this->load($default);
        }

    }

    function get($name) {
        return isset($this->language[$name]) ? $this->language[$name] : $name;
    }

    function __get($name) {
        return isset($this->language[$name]) ? $this->language[$name] : $name;
    }

    function set($name, $value) {
        $this->language[$name] = $value;
        return $this;
    }

    function __set($name, $value) {
        $this->language[$name] = $value;
    }

    function load($file) {
        $language = [];
        if (file_exists($this->path . '/' . $file . '.php')) {
            include($this->path . '/' . $file . '.php');
            $this->language = array_merge($this->language, $language);
            unset($language);
        }
    }
}