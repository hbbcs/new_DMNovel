<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-10-31
 * Time: 下午2:27
 */

if (!function_exists('site_url')) {
    function site_url($url)
    {
        $config = new \System\Config();
        return $config->baseUrl . $url;
    }
}
//------------------------------------------------------------------------
//创建多级目录
if (!function_exists('mkdirs')) {
    /**
     * @param string $dir
     * @param int $mode
     *
     * @return bool
     */
    function mkdirs($dir, $mode = 0777)
    {
        if (is_dir($dir) || @mkdir($dir, $mode)) {
            return true;
        }

        if (!mkdirs(dirname($dir), $mode)) {
            return false;
        }

        return @mkdir($dir, $mode);
    }
}
if (!function_exists('logs')) {

    function logs($message, $type = 'Error')
    {
        $log_file = fopen(BASE_PATH . '/Logs/' . date('Y-m-d') . '.php', 'a+');
        fputs($log_file, sprintf(date('H:i:s ') . " - %s: %s \n", $type, $message));
        fclose($log_file);
    }
}

if (!function_exists('showError')) {
    function showError($message, $header = 500, $title = '',$url=null)
    {
        $config = new \System\Config();
        switch ($header) {
            case 404:
                $header = "HTTP/1.0 404 Not Found";
                $title = "404 无法找到文件 ";
                break;
            case 405:
                $header = "HTTP/1.0 405 Method not allowed";
                $title = "405 资源被禁止 ";
                break;
            case 500:
                $header = "HTTP/1.0 500 Has An Error";
                $title = "500 内部服务器错误";
                break;
            default:
                $title = "发生错误";
                break;
        }
        header($header);

        $messages = $config->development ? new \System\Exception($message) : "";
        logs($messages);        
        $messages = str_replace(array("\r\n", "\r", "\n"), '<br/>', $messages);
        $viewPath = 'App/Views/error/';
        include $config->view['path'] . '/error/Error.html';
        exit(1);
    }
}

if (!function_exists('redirect')) {
    function redirect($url)
    {
        $regex = '@(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))@';
        $url = (preg_match($regex, $url)) ? $url : site_url($url);
        header("Location: " . $url);
        exit;
    }
}

if (!function_exists('charEncode')) {
    /**
     * @param string $data
     * @param string $to 输出字符串编码,默认'utf-8'
     *
     * @return string
     */
    function charEncode($data, $to = 'UTF-8')
    {
        $encode_arr = [
            'UTF-8',
            'ASCII',
            'GBK',
            'GB2312',
            'BIG5',
            'JIS',
            'eucjp-win',
            'sjis-win',
            'EUC-JP',
        ];
        $encoded = mb_detect_encoding($data, $encode_arr);
        $data = mb_convert_encoding($data, $to, $encoded);

        return $data;
    }
}

if (!function_exists('html_escape')) {
    /**
     * Returns HTML escaped variable.
     *
     * @param    mixed $var The input string or array of strings to be escaped.
     * @param    bool $double_encode $double_encode set to FALSE prevents escaping twice.
     *
     * @return    mixed            The escaped string or array of strings as a result.
     */
    function html_escape($var, $double_encode = true)
    {
        if (empty($var)) {
            return $var;
        }

        if (is_array($var)) {
            foreach (array_keys($var) as $key) {
                $var[$key] = html_escape($var[$key], $double_encode);
            }

            return $var;
        }

        return htmlspecialchars($var, ENT_QUOTES, config_item('charset'), $double_encode);
    }
}

if (!function_exists("cookie")) {
    function cookie($name,$value=null,$expire=null) {
        if ($value) {
            $today = strtotime(date("Y-m-d",time()));
            $today += 24*60*60;
            setcookie($name,$value,$expire?:$today);
        } else {
            return isset($_COOKIE[$name])?$_COOKIE[$name]:null;
        }
    }
}