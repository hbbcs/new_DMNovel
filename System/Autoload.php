<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-11-5
 * Time: 下午3:55
 */

spl_autoload_register(function ($class) {
    if ($class) {
        $file = str_replace('\\', '/', $class);
        $file .= '.php';
        if (file_exists($file)) {
            include $file;
        }
    }
});