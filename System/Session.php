<?php

namespace System;


class Session {

    private $expire = 7200;
    private $session_path = "assets/sessions";

    /**
     * Session constructor.
     * @param int $expire
     */
    public function __construct($expire = null) {
        $this->expire = $expire ? $expire * 3600 : 60 * 60 * 24 * 365;
    }

    public function start($expire = null) {
        $this->expire = $expire ? $expire * 3600 : 60 * 60 * 24 * 365;
        ini_set('session.gc_divisor', '1');
        ini_set('session.gc_probability', '1');
        ini_set('session.cookie_lifetime', '0');
        ini_set('session.gc_maxlifetime', $this->expire);
        ini_set('session.save_path', $this->session_path);
        session_start();
    }

    /**
     * @param int $expire
     */
    public function register($expire = 0) {
        $this->expire = $expire ?: $this->expire;

        if (!empty($_COOKIE['FISHSESSID'])) {
            session_set_cookie_params($this->expire);
        } else {
            setcookie('FISHSESSID', session_id(), time() + $this->expire);
        }
        $_SESSION['session_id'] = session_id();
        $_SESSION['session_time'] = intval($this->expire);
        $_SESSION['session_start'] = $this->newTime();
    }

    /**
     * Checks to see if the session is registered.
     *
     * @return  True if it is, False if not.
     */
    public function isRegistered() {
        if (!empty($_SESSION['session_id'])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Set key/value in session.
     *
     * @param mixed $key
     * @param mixed $value
     */
    public function set($key, $value) {
        $_SESSION[$key] = $value;
    }

    public function __set($name, $value) {
        $_SESSION[$name] = $value;
    }

    /**
     * @param $key
     * @return bool
     */
    function get($key) {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    public function __get($key) {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    public function has($key) {
        return isset($_SESSION[$key]);
    }

    public function remove($key) {
        if (is_array($key)) {
            foreach ($key as $k) {
                unset($_SESSION[$k]);
            }
            return;
        }
        unset($_SESSION[$key]);
    }

    /**
     * Retrieve the global session variable.
     *
     * @return array
     */
    public function getSession() {
        return $_SESSION;
    }


    /**
     * Checks to see if the session is over based on the amount of time given.
     *
     * @return boolean
     */
    public function isExpired() {
        if ($_SESSION['session_start'] < $this->timeNow()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Renews the session when the given time is not up and there is activity on the site.
     */
    public function renew() {
        $_SESSION['session_start'] = $this->newTime();
    }

    /**
     * Returns the current time.
     *
     * @return unix timestamp
     */
    private function timeNow() {
        return time();
    }

    /**
     * Generates new time.
     *
     * @return unix timestamp
     */
    private function newTime() {
        return time() + $_SESSION['session_time'];
    }

    /**
     * Destroys the session.
     */
    public function end() {
        session_destroy();
        $_SESSION = array();
    }
}