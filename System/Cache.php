<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-10-31
 * Time: 上午8:24
 */

namespace System;

class Cache
{
    /**
     * The path to the cache file folder
     *
     * @var string
     */
    private $_cachepath = 'assets/cache/';

    /**
     * The name of the default cache file
     *
     * @var string
     */
    private $_cachename = 'default';

    /**
     * The cache file extension
     *
     * @var string
     */
    private $_extension = '.cache';

    private $_expired = '7200';

    /**
     * Default constructor
     *
     * @param string|array [optional] $config
     * @return void
     */
    public function __construct($conf = null)
    {
        if (true === isset($conf)) {
            if (is_string($conf)) {
                $this->setCache($conf);
            } else if (is_array($conf)) {
                $this->setCache($conf['cache_name']);
                isset($conf['cache_path']) ? $this->setCachePath($conf['cache_path']) : "";
                isset($conf['cache_expired']) ? $this->setExpired($conf['cache_expired']) : "";
                isset($conf['cache_extension']) ? $this->setExtension($conf['cache_extension']) : "";
            }
        }
    }

    /**
     * Check whether data accociated with a key
     *
     * @param string $key
     * @return boolean
     */
    public function cached($key)
    {
        $this->eraseExpired();
        if (false != $this->_load_cache()) {
            $cachedData = $this->_load_cache();
            return isset($cachedData[$key]['data']);
        }
    }

    /**
     * Store data in the cache
     *
     * @param string $key
     * @param mixed $data
     * @param integer [optional] $expiration
     * @return object
     */
    public function set($key, $data, $expiration = 0)
    {
        $storeData = array(
            'time' => time(),
            'expire' => $expiration ?: $this->_expired,
            'data' => serialize($data),
        );
        $dataArray = $this->_load_cache();
        if (true === is_array($dataArray)) {
            $dataArray[$key] = $storeData;
        } else {
            $dataArray = array($key => $storeData);
        }
        $cacheData = json_encode($dataArray);
        file_put_contents($this->getCacheDir(), $cacheData);
        return $this;
    }

    public function setExpired($expired)
    {
        $this->_expired = $expired;
        return $this;
    }

    /**
     * Retrieve cached data by its key
     *
     * @param string $key
     * @param boolean [optional] $timestamp
     * @return string
     */
    public function get($key, $timestamp = false)
    {
        $cachedData = $this->_load_cache();
        $type = (false === $timestamp) ? 'data' : 'time';
        if (!isset($cachedData[$key][$type])) {
            return null;
        }
        return unserialize($cachedData[$key][$type]);
    }

    /**
     * Retrieve all cached data
     *
     * @param boolean [optional] $meta
     * @return array
     */
    public function getAll($meta = false)
    {
        if ($meta === false) {
            $results = array();
            $cachedData = $this->_load_cache();
            if ($cachedData) {
                foreach ($cachedData as $k => $v) {
                    $results[$k] = unserialize($v['data']);
                }
            }
            return $results;
        } else {
            return $this->_load_cache();
        }
    }

    /**
     * Erase cached entry by its key
     *
     * @param string $key
     * @return object
     */
    public function erase($key)
    {
        $cacheData = $this->_load_cache();
        if (true === is_array($cacheData)) {
            if (true === isset($cacheData[$key])) {
                unset($cacheData[$key]);
                $cacheData = json_encode($cacheData);
                file_put_contents($this->getCacheDir(), $cacheData);
            } else {
                showError("Error: erase() - Key '{$key}' not found.");
            }
        }
        return $this;
    }

    /**
     * Erase all expired entries
     *
     * @return integer
     */
    public function eraseExpired()
    {
        $cacheData = $this->_load_cache();
        $counter = 0;
        if (true === is_array($cacheData)) {
            foreach ($cacheData as $key => $entry) {
                if (true === $this->_check_expired($entry['time'], $entry['expire'])) {
                    unset($cacheData[$key]);
                    $counter++;
                }
            }
            if ($counter > 0) {
                $cacheData = json_encode($cacheData);
                file_put_contents($this->getCacheDir(), $cacheData);
            }
        }
        return $counter;
    }

    /**
     * Erase all cached entries
     *
     * @return object
     */
    public function eraseAll()
    {
        $cacheDir = $this->getCacheDir();
        if (true === file_exists($cacheDir)) {
            $cacheFile = fopen($cacheDir, 'w');
            fclose($cacheFile);
        }
        return $this;
    }

    /**
     * Load appointed cache
     *
     * @return mixed
     */
    private function _load_cache()
    {
        if (true === file_exists($this->getCacheDir())) {
            $file = file_get_contents($this->getCacheDir());
            return json_decode($file, true);
        } else {
            return false;
        }
    }

    /**
     * Get the cache directory path
     *
     * @return string
     */
    public function getCacheDir()
    {
        if (true === $this->_check_cache_dir()) {
            $filename = $this->getCache();
            $filename = preg_replace('/[^0-9a-z\.\_\-]/i', '', strtolower($filename));
            return $this->getCachePath() . $this->_get_hash($filename) . $this->getExtension();
        }
    }

    /**
     * Get the filename hash
     *
     * @return string
     */
    private function _get_hash($filename)
    {
        return sha1($filename);
    }

    /**
     * Check whether a timestamp is still in the duration
     *
     * @param integer $timestamp
     * @param integer $expiration
     * @return boolean
     */
    private function _check_expired($timestamp, $expiration)
    {
        $result = false;
        if ($expiration !== 0) {
            $timeDiff = time() - $timestamp;
            $result = ($timeDiff > $expiration) ? true : false;
        }
        return $result;
    }

    /**
     * Check if a writable cache directory exists and if not create a new one
     *
     * @return boolean
     */
    private function _check_cache_dir()
    {
        if (!is_dir($this->getCachePath()) && !mkdir($this->getCachePath(), 0775, true)) {
            showError('Unable to create cache directory ' . $this->getCachePath());
        } elseif (!is_readable($this->getCachePath()) || !is_writable($this->getCachePath())) {
            if (!chmod($this->getCachePath(), 0775)) {
                showError($this->getCachePath() . ' must be readable and writeable');
            }
        }
        return true;
    }

    /**
     * Cache path Setter
     *
     * @param string $path
     * @return object
     */
    public function setCachePath($path)
    {
        $this->_cachepath = $path;
        return $this;
    }

    /**
     * Cache path Getter
     *
     * @return string
     */
    public function getCachePath()
    {
        return $this->_cachepath;
    }

    /**
     * Cache name Setter
     *
     * @param string $name
     * @return object
     */
    public function setCache($name)
    {
        $this->_cachename = $name;
        return $this;
    }

    /**
     * Cache name Getter
     *
     * @return string
     */
    public function getCache()
    {
        return $this->_cachename;
    }

    /**
     * Cache file extension Setter
     *
     * @param string $ext
     * @return object
     */
    public function setExtension($ext)
    {
        $this->_extension = $ext;
        return $this;
    }

    /**
     * Cache file extension Getter
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->_extension;
    }

}
