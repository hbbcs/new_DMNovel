<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-11-6
 * Time: 下午4:52
 */

namespace System;
class Exception extends \Exception {
    private $previous;

    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code);

        if (!is_null($previous)) {
            $this->previous = $previous;
        }
    }
}