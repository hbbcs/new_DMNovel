<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-10-31
 * Time: 上午8:57
 */

namespace System;

class Request
{
    public $get, $post, $request, $cookie;

    public function __construct()
    {
        $_GET = array_map(array(__CLASS__, 'userInput'), $_GET);
        $_POST = array_map(array(__CLASS__, 'userInput'), $_POST);
        $_COOKIE = array_map(array(__CLASS__, 'userInput'), $_COOKIE);
        $_REQUEST = array_map(array(__CLASS__, 'userInput'), $_REQUEST);
    }

    public function isAjax()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? true : false;
    }

    public function get($name = null)
    {
        return $name ? (isset($_GET[$name]) ? $_GET[$name] : null) : $_GET;
    }

    public function post($name = null)
    {
        return $name ? (isset($_POST[$name]) ? $_POST[$name] : null) : $_POST;
    }

    public function request($name = null)
    {
        return $name ? (isset($_REQUEST[$name]) ? $_REQUEST[$name] : null) : $_REQUEST;
    }

    public function cookie($name, $value = null, $expire = '', $path = '/', $domain = '', $secure = null)
    {
        if ($value) {
            return setcookie($name, $value, $expire, $path, $domain, $secure);
        } else {
            return isset($_COOKIE[$name]) ? $_COOKIE[$name] : null;
        }
    }

    private function userInput($input)
    {
        if (!isset($input)) {
            return null;
        }

        $search = array("\\", "\x00", "\n", "\r", "'", '"', "\x1a");
        $replace = array("\\\\", "\\0", "\\n", "\\r", "\'", '\"', "\\Z");
        if (is_array($input)) {
            foreach ($input as $key) {
                if (get_magic_quotes_gpc()) {
                    $return[] = str_replace($search, $replace, stripslashes($this->safe($key)));
                } else {
                    $return[] = str_replace($search, $replace, $this->safe($key));
                }
            }
            return $return;
        } else {
            if (get_magic_quotes_gpc()) {
                return str_replace($search, $replace, stripslashes($this->safe($input)));
            } else {
                return str_replace($search, $replace, $this->safe($input));
            }
        }
    }

    private function safe($str)
    {
        if (!isset($str)) {
            return null;
        }

        $reg=[
            '/(<\s*SCRIPT|SCRIPT\s*>)/i',
            '/(<\s*IFRAME|IFRAME\s*>)/i',
            '/(UNION|SELECT|CONCAT|DELETE|INSERT|DROP|FROM|WHERE) /i',
            '/(UNION|SELECT|CONCAT|DELETE|INSERT|DROP|FROM|WHERE)\(/i',
            '/\/\*/i',
            '/\-\-/i',
        ];

        if (!is_array($str)) {
            $decode = $this->decode($str);
            foreach ($reg as $r) {
                if (preg_match($r, $decode)) {
                    return null;
                }
            }            
        }
        return $str;
    }

    public function encode($data)
    {
        if (is_array($data)) {
            return array_map(array($this, 'encode'), $data);
        }

        if (is_object($data)) {
            $tmp = clone $data;
            foreach ($data as $k => $var) {
                $tmp->{$k} = $this->encode($var);
            }

            return $tmp;
        }
        return htmlentities($data);
    }

    public function decode($data)
    {
        if (is_array($data)) {
            return array_map(array($this, 'decode'), $data);
        }

        if (is_object($data)) {
            $tmp = clone $data;
            foreach ($data as $k => $var) {
                $tmp->{$k} = $this->decode($var);
            }

            return $tmp;
        }
        return html_entity_decode($data);
    }

}