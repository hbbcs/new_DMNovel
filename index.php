<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-11-5
 * Time: 下午3:20
 */

ini_set('display_errors',1);
ini_set('display_startup_errors',1);

//error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL);
define('BASE_PATH', __DIR__);

include 'System/Autoload.php';
include 'System/Core.php';


$route= new  \System\Router();
$route->run();