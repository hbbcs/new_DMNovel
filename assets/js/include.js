(function ($) {
    $.include = {
        path: "",
        set: function (path) {
            this.path = path;
            return this;
        },
        file: function (files) {
            files = typeof files == "string" ? [files] : files;
            path = this.path;


            $.each(files, function (key, v) {
                var name = v.replace(/^\s$/g, "");
                var att = name.split('.');
                var ext = att[att.length - 1].toLowerCase();
                var isCSS = ext == "css";
                var link;
                if (isCSS) {
                    link = $("<link>", {
                        "type": "text/css",
                        "rel": "stylesheet",
                        "href": path + "css/" + name
                    });
                    if ($("link[href='" + path + "css/" + name + "']").length == 0)
                        $("head").append(link);
                } else {
                    link = $("<script>", {
                        "language": "javascript",
                        "type": "text/javascript",
                        "src": path + "js/" + name
                    });
                    if ($("script[src='" + path + "css/" + name + "']").length == 0)
                        $("body").append(link);
                }
            });

        }
    }
})(jQuery);