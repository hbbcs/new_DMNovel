(function ($) {
    $.fn.extend({
        Pagination: function (options) {
            var options = $.extend({
                width: $(this).width(),
                height: $(window).height() - 130,
                fadeSpeed: 200,
            }, options || {});

            var $content = $(this);

            var chapterID = $("#cent").data('chapter');

            var lastPage;
            var cPage = (store("readPage_" + chapterID)) ? parseInt(store("readPage_" + chapterID)) : 1;
            var url = $('#pagination').attr("url");

            setContent = function () {
                $content.addClass('content');

                if ($content.find('img').length > 0) {
                    $content.find('img').each(function (e) {
                        var img = $(this);
                        if (img[0].offsetHeight > options.height) {
                            img.css('height', options.height)
                        } else if (img[0].offsetWidth > options.width) {
                            img.css('width', options.width - 30)
                        }
                    });
                }
                lastPage = Math.ceil($content.outerHeight() / (options.height - 30));
                $content.height(options.height);
                //$content.css("column-width", options.width + 'px');
                //$content.css("-moz-column-width", options.width + 'px');
                //$content.css("-webkit-column-width", options.width + 'px');
            };

            setAllPage = function () {
                lastPage = Math.ceil($content.outerHeight() / (options.height - 30));
            }

            showPage = function (page) {
                var chapterID;
                if (page < 1) {
                    chapterID = $("#prev").data("chapter");
                } else if (page > lastPage) {
                    chapterID = $("#next").data("chapter");
                }

                if (chapterID == -1 || chapterID == 0) {
                    $.toast({
                        position: 'top-center',
                        text: "没有内容了...",
                        icon: "warning"
                    });
                    return;
                } else if (chapterID > 0) {
                    window.location.href = url + "/" + chapterID;
                    return;
                }


                cPage = page;

                store("readPage_" + chapterID, cPage);

                var scrollLeft = (page - 1) * (options.width + 50);

                $content.animate({
                    scrollLeft: scrollLeft
                }, options.fadeSpeed);

                $("#cPage").html(page + '/' + lastPage);
            };

            setContent();
            showPage(cPage);

            $('#prev').mousedown(function () {
                showPage(cPage - 1);
            });

            $(document).keyup(function (e) {
                var key = e.which;
                if (key === 37 || key === 38) {
                    showPage(cPage - 1);
                } else if (key === 39 || key === 40) {
                    showPage(cPage + 1);
                }
            });

            $('.chapter').bind("mousewheel DOMMouseScroll", function (event) {
                var delta = (event.wheelDelta) ? event.wheelDelta / 120 : -(event.detail || 0) / 3;
                showPage(cPage - delta);
            });

            // and Next
            $('#next').mousedown(function () {
                showPage(cPage + 1);
            });

        }
    });
})(jQuery);