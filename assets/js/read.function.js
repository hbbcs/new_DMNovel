var setFontSize = function (size) {
    $("#fontSize").text(size);
    $('#content').css("font-size", size + "px");
}

var setLineHeight = function (size) {
    $("#lineHeight").text(size);
    $('#content').find("p").css("line-height", size + "em");
}

var setChapterListHeight = function () {
    var h = window.innerHeight || document.body.clientHeight || document.documentElement
        .clientHeight;
    $(".chapterList").css("height", h);
}

var setBackground = function (color) {
    $('#setBackground').find(".corner").remove();
    $("#setBackground .icon").each(function () {
        if ($(this).css("color") == color)
            $(this).parent().append('<i class="corner check icon"></i>');
    })
    $("#content").parent(".message").css("background", color);
    $("#content").css("background", color);
    store("background", color);
}


$(function () {
    var background = store("background") ? store("background") : "#FAF9DE";
    setBackground(background);

    $('#setBackground .icon').click(function () {
        var color = $(this).css("color");
        setBackground(color);
    });

    $(window).on("load resize", function () {
        setChapterListHeight();
    });

    var fontSize = store("fontSize") ? store("fontSize") : 18;
    setFontSize(fontSize);
    var lineHeight = store("lineHeight") ? store("lineHeight") : 1.5;
    setLineHeight(lineHeight);

    $('#config').popup({
        popup: '.special.popup',
        on: 'click',
        position: "bottom right"
    });

    $('#setLineHeight .button').click(function () {
        var lineHeight = parseFloat($("#lineHeight").text());
        if ($(this).hasClass("add")) {
            lineHeight += 0.5;
        } else if ($(this).hasClass("minus") && lineHeight > 0.5) {
            lineHeight -= 0.5;
        }
        setLineHeight(lineHeight);
        store("lineHeight", lineHeight);
    })

    $('#setFontSize .button').click(function () {
        var size = parseInt($("#fontSize").text());
        if ($(this).hasClass("add")) {
            size += 2;
        } else if ($(this).hasClass("minus") && size > 10) {
            size -= 2;
        }
        setFontSize(size);
        store("fontSize", size);
    });

    $("#chapterList,#showChapterList,#cent").click(function () {
        if ($(".chapterList").data("show")) {
            $(".chapterList").animate({
                left: '-320px'
            }).data("show", false);
        } else {
            $(".chapterList").animate({
                left: '0px'
            }).data("show", true);
        }
    });

})