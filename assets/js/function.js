//基本加载项
$(function () {
    $(document).on('click', '[data-pjax]', function (event) {
        alert("正在加载，请稍候...<i class='icon spinner loading '></i>");
        var con = $(this).data('pjax');
        var containerSelector = '#' + (con && con != "true" ? con : 'mainContent');
        $.pjax.click(event, {
            container: containerSelector
        })
    });

    $(document).on('pjax:start', function () {
        NProgress.start();
    }).on('pjax:end', function () {
        NProgress.done();
    });

    $(document).on('click', '[data-dialog]', function (e) {
        e.preventDefault();
        var url = $(this).data("dialog") != "" ? $(this).data("dialog") : $(this).attr("href");
        var title = $(this).attr("title") != "" ? $(this).attr("title") : $(this).html();
        var size = $(this).data("dialog-size");
        var id = $(this).attr("id") ? $(this).attr('id') : $(this).data("dialog-id");
        dialog({
            "title": title,
            "url": url,
            "size": size
        })
    });
});

var dialog = function (settings) {
    settings = $.extend({
        'close': true,
        'id': 'modal_ajax',
        'size': 'modal-lg', // modal-sm,modal-lg,modal-xl
        'title': '',
        'icon': '',
        'url': '',
        'content': '',
        'actions': {},
    }, settings || {});

    var modalT = $("<div>", {
        "class": "modal " + settings.size,
        "role": "dialog",
        "tabindex": "-1",
        "id": settings.id
    });

    var content = $("<div>", { "class": "modal-content" });

    if (settings.title) {
        var header = $('<div>', {
            'class': "modal-header",
            'html': "<h5 class='modal-title h4'>" + settings.title + "</h5>"
        })

        if (settings.close) {
            header.append('<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                '<span aria-hidden="true">×</span>' +
                '</button>');
        }
        content.append(header);
    }

    var body = $("<div>", {
        "class": "modal-body",
        "html": settings.content
    });

    if (settings.url) {
        $.get(settings.url,
            function (data) {
                body.html(data);
            }
        );
    }
    content.append(body);
    console.log($.isEmptyObject(settings.actions))
    if (!$.isEmptyObject(settings.actions)) {
        var actions = $("<div>", {
            "class": "modal-footer"
        });

        $.each(settings.actions, function (actionName, action) {
            actions.append($("<div>", {
                "class": "button " + action.class,
                "text": action.name ? action.name : actionName,
                "id": actionName
            }));

            $(document).off("click", "#" + actionName)
                .on("click", "#" + actionName, function () {
                    if (typeof action.action == "function") action.action();
                    $("#" + settings.id).modal("hide").remove();
                });

        });
        content.append(actions);
    }
    console.log(content)
    modalT.append(content);
    modalT.appendTo("body")
        .modal({
            "onHidden": function () {
                $("#" + settings.id).remove();
            }
        })
        .modal("show");
    modalT.on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}

dialog.close = function (id) {
    $("#" + id).remove();
}

var alert = function () {
    message = arguments[0];
    icon = arguments[1] ? arguments[1] : "info";
    $.toast({
        'heading': "信息",
        'position': 'top-center',
        'text': "<h2 class=\"center\">" + message + "</h2>",
        'showHideTransition': 'slide',
        'icon': icon
    })
}

var confirms = function (settings) {
    settings = $.extend({
        'close': false,
        'id': 'confirm_message',
        'size': 'small', // mini,tiny,small,large,fullscreen
        'title': '',
        'icon': 'exclamation triangle',
        'content': '',
        'basic': true,
        'okBtn': {},
        'cancelBtn': {}
    }, settings || {});

    settings.content = "<h2 class='ui center aligned red header'>" + settings.content + "</h2>";

    settings.actions = {
        'okBtn': {
            'class': settings.okBtn.class ? settings.okBtn.class : "green",
            'name': settings.okBtn.name ? settings.okBtn.name : "OK",
            'action': settings.okBtn.action
        },
        "cancelBtn": {
            'class': settings.cancelBtn.class ? settings.cancelBtn.class : "teal",
            'name': settings.cancelBtn.name ? settings.cancelBtn.name : 'CANCEL',
            'action': settings.cancelBtn.action
        }
    }
    dialog(settings);
}

var store = function () {
    if (typeof (arguments[0]) == 'object') {
        arguments[0].each(function (name, val) {
            localStorage.setItem(name, val);
        })
    } else if (arguments[1]) {
        localStorage.setItem(arguments[0], arguments[1]);
    } else {
        return localStorage.getItem(arguments[0]) ? localStorage.getItem(arguments[0]) : null;
    }
};

$.fn.Scroll = function () {
    /*this.find('ul').animate({
        marginTop: "-15px"
    }, 500, function () {
        $(this).css("marginTop", "0px").find("li:first").appendTo(this)
    });*/

};

$.fn.imageLoad = function () {
    this.each(function () {
        var oT = $(this).offset().top;
        var sT = $(window).scrollTop();
        var cH = $(window).height();
        if (sT + cH >= oT) {
            var s = $(this).data('src');
            $(this).attr('src', s);
            $(this).data('isLoaded', 1);
        }
    });
}