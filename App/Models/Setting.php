<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 19-4-17
 * Time: 上午11:18
 */

namespace App\Models;

use System\Model;

class Setting extends Model
{
    protected $table = "setting";
    protected $id = 'id';
    protected $fields = ['id', 'name', 'value', 'desc', 'type', 'options'];

    public function getSetting($name, $return = 'value')
    {
        $setting = $this->where('name', $name)->first();
        if ($return == 'value') {
            return $setting['value'];
        } else {
            return $setting;
        }
    }
}
