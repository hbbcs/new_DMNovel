<?php namespace App\Models;

use \System\Model;

class Notice extends Model
{
    protected $table = "notice";
    protected $id = 'id';
    protected $fields = ['id', 'content', 'user',  'date'];
}