<?php namespace App\Models;

use \System\Model;

class Comment extends Model
{
    protected $table = "comment";
    protected $id = 'id';
    protected $fields = ['id', 'story', 'user',  'content', 'date'];
}