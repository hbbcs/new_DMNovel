<?php namespace App\Models;

use System\Model;

class Stat extends Model {
    protected $table  = "stat";
    protected $id='id';
    protected $fields = ['id','date', 'count'];
}