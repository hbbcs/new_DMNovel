<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-12-1
 * Time: 下午4:49
 */

namespace App\Models;

use \System\Model;

class Search extends Model
{
    protected $table = "search";
    protected $id = 'id';
    protected $fields = ['id', 'title', 'count'];
}
