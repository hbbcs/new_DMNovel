<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-12-1
 * Time: 下午2:41
 */

namespace App\Models;

use \System\Model;

class Group extends Model
{
    protected $table = "groups";
    protected $id = 'id';
    protected $fields = ['id', 'name', 'level'];
}