<?php namespace App\Models;

use \System\Model;

class Favorites extends Model
{
    protected $table = "favorites";
    protected $id = 'id';
    protected $fields = ['id', 'story', 'user'];

    public function findByUser($userid,$page=null,$story=false)
    {
        if ($page!=null) {
            $this->limit(20,$page*20);
        }
        if ($story) {
            $this->join("story","story.id=favorites.story")->select("story.*")->order("story.last_update","DESC");
        }
        return $this->where("user",$userid)->findAll();
    }

    public function check($userid,$storyid)
    {
        return $this->where(["user"=>$userid,"story"=>$storyid])->first()?:false;
    }
}