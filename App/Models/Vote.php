<?php namespace App\Models;

use \System\Model;

class Vote extends Model
{
    protected $table = "votes";
    protected $id = 'id';
    protected $fields = ['id', 'user', 'num', 'reset'];

    public function reset($userID)
    {
        $vote = $this->where('user', $userID)->first();

        if (!$vote) {
            $vote = [
                'user' => $userID,
                'num' => 20,
                "reset" => time()
            ];
            $this->save($vote);
        } else if (date("m", $vote['reset']) < date("m")) {
            $vote['num'] = 20;
            $vote['reset'] = time();
            $this->save($vote);
        }
        return $vote;
    }
}