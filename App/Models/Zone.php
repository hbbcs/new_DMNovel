<?php namespace App\Models;

use \System\Model;

class Zone extends Model
{
    protected $table = "zone";
    protected $id = 'id';
    protected $fields = ['id', 'parent', 'name'];
}