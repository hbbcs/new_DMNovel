<div class="ui raised segment">
    <div class="ui text shape">
        <div class="sides">
            {$i = 0}
            {if count($notices)>0}
            {foreach $notices as $notice}
            <div class="ui header side <?=$i == 0 ? 'active' : ''?>">
                <?=date("m月d日", $notice['date'])?>：{$notice['content']}</div>
            {$i++}
            {/foreach}
            {else}
            <div class="ui header">暂无公告</div>
            {/if}
        </div>
    </div>
</div>

<div class="ui grid">
    <div class="row">
        <div class="ui three wide column">
            <div class="ui attached fluid segments">
                <div class="ui segment header">
                    <i class="icon th list"></i>
                    作品分类
                </div>
                <div class="ui horizontal segments">
                    {if isset($categories)}
                    <?php $i = 0;
$count = count($categories);?>
                    {foreach $categories as $category}
                    <div class="ui segment center">
                        <h5>
                            <i class="{$category['icon']}"></i>
                            <a href="{url('/category/'.$category['id'])}" data-pjax="">{$category['name']}</a>
                        </h5>
                    </div>
                    {$i++}
                    {if $i%2==0 && $i<$count} </div>
                        <div class="ui horizontal segments">

                            {/if}
                            {/foreach}
                            {/if}
                        </div>
                </div>
            </div>
            <div class="ui thirteen wide column">
                <div class="ui four cards">
                    {foreach $hots as $story}
                    <div class="card" data-title="{$story['title']}-{$story['author']}" data-content="{$story['desc']}"
                        data-position="right center">
                        <a class="image" href="{url('/book/')}{$story['id']}" data-pjax="">
                            <img src="{$assets}/images/covers/{$story['cover']}">
                        </a>
                        <div class="extra content">
                            <span title="收藏">
                                <i class="icon-shelf"></i>
                                {$story['favorites']}
                            </span>
                            <span class="right floated" title="得票">
                                <i class="star icon"></i>
                                {$story['vote']}
                            </span>
                        </div>
                    </div>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>


    <div class="ui grid" style="margin-top:10px;">
        <div class="row">
            <div class="ui twelve wide column">
                <div class="ui segment">
                    <div class="ui three column grid">
                        {foreach $categories as $category}
                        <div class="column">
                            <div class="ui horizontal divider header">
                                <i class="icon {$category['icon']}"></i>
                                {$category['name']}
                            </div>
                            <div class="ui items category" data-id="{$category['id']}">
                                <div class="ui active inverted dimmer">
                                    <div class="ui text loader">Loading</div>
                                </div>
                                <p></p>
                            </div>
                        </div>
                        {/foreach}
                    </div>
                </div>
            </div>
            <div class="ui four wide column">
                <div class="ui attached fluid segments">
                    <div class="ui segment header">
                        <i class="icon target"></i>
                        点击排行
                    </div>
                    <div class="ui segment">
                        <div class="ui relaxed divided ordered list">
                            {foreach $clicks as $story}
                            <a href="{url('/book/')}{$story['id']}" class="item" data-pjax="">
                                {$story['title']}
                                <label class="right floated">
                                    {$story['click']}
                                </label>
                            </a>
                            {/foreach}
                        </div>
                    </div>
                </div>
                <div class="ui attached fluid segments">
                    <div class="ui segment header">
                        <i class="icon sync alternate"></i>
                        最后更新
                    </div>
                    <div class="ui segment">
                        <div class="ui relaxed divided list">
                            {foreach $last as $story}
                            <a href="{url('/book/')}{$story['id']}" class="item" data-pjax="">
                                {$story['title']}
                                <label class="right floated">
                                    <?=date("m-d", $story['last_update'])?>
                                </label>
                            </a>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>




    <script type="text/javascript">
    $(function() {
        $.include.set("{$assets}/").file(["jquery.Timer.js"]);
        $(".card").popup();
        $('body').everyTime('10s', function() {
            $('.shape').shape('flip down');
        });
        $(".category").each(function() {
            var $this = $(this);
            var id = $(this).data("id");

            $.get("{url('/hot/')}" + id + "/5", function(data) {
                $this.html("")
                if (data) {
                    $.each($.parseJSON(data), function(k, v) {
                        var item = $("<div>", {
                            "class": "item"
                        }).append(
                            $("<div>", {
                                "class": "content"
                            }).html("<a class='meta' href='{url('/book/')}" + v.id +
                                "' data-pjax=''>" +
                                v.title + "</a>")
                        );
                        if (k == 0) {
                            item.prepend(
                                    '<div class = "ui tiny image" ><img src="{$assets}/images/covers/' +
                                    v.cover + '"></div>')
                                .find(".content").append(
                                    '<div class="description half"> <p>' + v.desc +
                                    '</p> </div>'
                                )
                        } else {
                            item.find(".meta").append("<label class='right floated'>" +
                                v.vote +
                                "</label>")
                        }
                        $this.append(item);
                    })
                }
            });
        })
    });
    </script>