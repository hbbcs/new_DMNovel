<div class="ui grid h100" style="height:100%">
    <div class="three wide column">
        <div class="ui sticky">
            <div class="ui vertical pointing menu" id="profileMenu">
                <a class="active item" href="{url('/user/profile')}">
                    我的属性
                </a>
                <a class="item" href="{url('/user/friend')}">
                    我的好友
                </a>
                <a class="item" href="{url('/user/password')}">
                    修改密码
                </a>
            </div>
        </div>
    </div>
    <div class="thirteen wide stretched column">
        <div class="ui segment" style="height:100%" id="profileContent">
            正在加载，请稍候...
        </div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.ui.sticky').sticky({
        offset: 66,
        context: '#profileContent'
    });
    var href = $('#profileMenu').find(".active").attr('href');
    $("#profileContent").addClass('loading').load(href).removeClass("loading");

    $('#profileMenu a').click(function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        $('#profileMenu').find(".active").removeClass("active");
        $(this).addClass('active');
        $("#profileContent").addClass('loading').load(href).removeClass("loading");
    });
});
</script>