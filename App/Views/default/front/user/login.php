<div class="row" style="margin:0 auto; width:400px; height:100%;">
    <div class="ui middle aligned">

        <div class="login-box">
            <h2 class="ui teal image header">
                <img src="{$assets}/images/logo-mini.png" class="image">
                <div class="content">
                    登录到账号
                </div>
            </h2>
            <form class="ui small form" action="{url('/user/login')}" method="post" id="loginForm">
                <div class="ui stacked segment">
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="username" placeholder="用户名" data-required='true'>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" placeholder="密码" data-required='true'>
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large teal submit button">登录</button>
                </div>
            </form>
            <div class="ui message">
                <div class="ui left labeled button" tabindex="0">
                    <span class="ui basic right pointing label">
                        新用户?
                    </span>
                    <div class="ui button" data-dialog="{url('/user/register')}" title="注册新用户">
                        <i class="user add icon"></i> 注册
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $.include.set("{$assets}/").file("jquery.validator.js");

    $('#loginForm').validator({
        success: function(data) {
            $.toast({
                heading: data.status,
                position: 'top-center',
                text: data.message,
                icon: data.status == "Error" ? "warning" : "success"
            });
            if (data.status != "Error") {
                window.location.href = '{url("/")}';
            }
        }
    });
});
</script>