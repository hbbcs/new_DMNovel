<div class="ui breadcrumb">
    <a class="section" href="{url('/')}" data-pjax="">首页</a>
    <i class="right chevron icon divider"></i>
    <div class="active section">我的书架</div>
</div>

<div class="ui segment" style="min-height:300px;">
    <div class="ui five cards" id="rankList">
        {if count($stories)>0}
        {foreach $stories as $story}
        <div class="ui card" data-story-id="{$story['id']}">
            <div class="image">
                <img src="{$assets}/images/covers/{$story['cover']}" />
            </div>
            <div class="content">
                <a class="header">{$story['title']}</a>
                <div class="meta">
                    <span class="date"><?=date("m月d日 h:i",$story['last_update'])?></span>
                </div>
                <div class="description">
                    <a href="{url('/read')}/{$story['id']}/{$story['last_chapter_id']}">
                        {$story['last_chapter']}
                    </a>
                </div>
            </div>
            <div class="extra content">
                <a class="shelf">
                    <i class="icon-shelf"></i>
                    {$story['favorites']}
                </a>
            </div>
        </div>

        {/foreach}
        {else}
        <h2 class="ui center aligned icon header">
            <i class="icon icon-info"></i>
            您还没有收藏任何小说...没有感兴趣的吗？
        </h2>
        {/if}
    </div>
</div>

<script type="text/javascript">
$(function() {
    $(".shelf").click(function() {
        var item = $(this).parents(".ui.card");
        var storyid = item.data("storyid");
        $.get("{url('/shelf/favorites/')}" + storyid, function(data) {
            data = $.parseJSON(data);
            alert(data.message, data.status);
            if (data.status != "error")
                item.remove();
        })
    })
});
</script>