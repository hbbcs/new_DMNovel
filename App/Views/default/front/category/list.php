{foreach $stories['books'] as $story}
<div class="item story">
    <img src="{$assets}/images/covers/default.jpg"
        data-src="<?= ($type != 'cloud') ? $assets . '/images/covers/' : $staticUrl ?>{$story['cover']}"
        class="miniCover" />
    <div class="content">
        <a class="header <?= $type == 'local' ?: 'cloud' ?>"
            href="{url('/book/')}<?= isset($story['id']) ? $story['id'] : $story['_id'] ?><?= $type == 'local' || !$type ? "" : '/cloud' ?>"
            data-pjax=true>
            <h3>{$story['title']}</h3>
        </a>
        <div class="description">
            <div class="ui labeled mini button" tabindex="0" title="作者">
                <div class="ui mini icon button">
                    <i class="icon user"></i>
                </div>
                <a class="ui basic left pointing label" href="{url('/search')}?key={$story['author']}" data-pjax=true>
                    {$story['author']}
                </a>
            </div>
            {if $type=="cloud"}
            <div class="ui labeled mini button" tabindex="0" title="留存率">
                <div class="ui mini icon button">
                    <i class="icon heart"></i>
                </div>
                <a class="ui basic left pointing label">
                    {$story['retentionRatio']}
                </a>
            </div>
            <div class="ui labeled mini button" tabindex="0" title="追书者">
                <div class="ui mini icon button">
                    <i class="icon flag"></i>
                </div>
                <a class="ui basic left pointing label">
                    {$story['latelyFollower']}
                </a>
            </div>
            {/if}
        </div>
        <div class="description">
            简介：<?= isset($story['shortIntro']) ? $story['shortIntro'] : $story['desc'] ?>
        </div>
    </div>
</div>
{/foreach}
{if $page==0}
<script type="text/javascript">
$(function() {
    $(".miniCover[data-isLoaded!=1]").imageLoad();
    $("#categoryBooksList").data("total", <?= $stories['total'] ?>).data("type", '<?= $type ?: "local" ?>')
});
</script>
{/if}