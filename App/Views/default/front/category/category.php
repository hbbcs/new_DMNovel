<div class="ui breadcrumb">
    <a class="section" href="{url('/')}" data-pjax="">首页</a>
    {foreach $categoryTitle as $t}
    <i class="right chevron icon divider"></i>
    <a class="section" href="{url('/category/')}{$t['id']}" data-pjax="">{$t['name']}</a>
    {/foreach}
</div>

<div class="ui pointing secondary menu" id="categoryMenu">
    <a class="item active" data-type="local">
        <i class="icon server"></i>
        本地
    </a>
    <a class="item" data-type="cloud">
        <i class="icon cloud"></i>
        追书
    </a>
</div>

<div class="ui grid">
    <div class="<?=count($sub) > 0 ? "fourteen" : "sixteen"?> wide column">
        <div class="ui segment">
            <div class="ui relaxed divided list" id="categoryBooksList" data-total="{$stories['total']}"
                data-page="{$page}" data-type="{$type}">
                <?=$list ?: "<h1>当前分类没有小说</h1>"?>
            </div>
        </div>
    </div>
    {if count($sub)>0}
    <div class="two wide column">
        <div class="ui segments sticky" id="subCateMenu">
            <div class="ui segment header">
                <i class="icon th list"></i>
                子分类
            </div>

            {foreach $sub as $s}
            <div class="ui segment">
                <h5>
                    <a href="{url('/category/'.$s['id'])}" data-pjax="true">{$s['name']}</a>
                </h5>
            </div>
            {/foreach}
        </div>
    </div>
    {/if}
</div>

<script type="text/javascript">
$(function() {
    //$("#categoryList").load("{url('/category/get/')}{$category}");
    $(window).scroll(function() {
        $(".miniCover[data-isLoaded!=1]").imageLoad();
    });

    $('#categoryMenu a').click(function() {
        $('#categoryMenu').find(".active").removeClass("active");
        $(this).addClass("active");
        var type = $(this).data("type");
        console.log(type);

        $("#categoryBooksList").data("type", type).addClass("loading");
        $.post("{url('/category/get/')}{$category}", {
            page: 0,
            'type': type
        }, function(data) {
            $("#categoryBooksList").html(data ? data : "<h1>当前分类没有小说</h1>")
                .removeClass("loading");
        });
    })

    $(document).scroll(function() {
        $(".miniCover[data-isLoaded!=1]").imageLoad();
        var htmlHeight = document.body.scrollHeight || document.documentElement.scrollHeight;
        //clientHeight是网页在浏览器中的可视高度，
        var clientHeight = document.body.clientHeight || document.documentElement.clientHeight;
        //scrollTop是浏览器滚动条的top位置，
        var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
        var allPage = Math.ceil(parseInt($('#categoryBooksList').data("total")) / 20)
        var page = parseInt($('#categoryBooksList').data("page")) + 1;
        var type = $('#categoryBooksList').data("type");

        var message;
        //通过判断滚动条的top位置与可视网页之和与整个网页的高度是否相等来决定是否加载内容；
        if (scrollTop + clientHeight == htmlHeight) {
            if (allPage > page) {
                message = "正在加载，请稍候..."
                $.post("{url('/category/get/')}{$category}?", {
                    'type': type ? type : "local",
                    'page': page,
                }, function(data) {
                    $('#categoryBooksList').data("page", page).append(data);
                });
            } else {
                message = "已全部加载完毕";
            }
            alert(message, "success");
        }
    });
});
</script>