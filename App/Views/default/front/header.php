<!DOCTYPE html>
<html>

<head>
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <title><?=isset($viewTitle) ? $viewTitle . " - " : "";?>{$title}</title>
    <link rel="stylesheet" type="text/css" href="{$assets}/css/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="{$assets}/css/main.css">
    <link rel="stylesheet" type="text/css" href="{$assets}/css/iconfont.css">
    <link rel="stylesheet" type="text/css" href="{$assets}/css/nprogress.css">
    <link rel="stylesheet" type="text/css" href="{$assets}/css/jquery.toast.min.css">
    <script type="text/javascript" src="{$assets}/js/jquery.min.js"></script>
</head>

<body>
    <!-- Menu content -->
    <div class="ui blue fixed pointing borderless inverted menu">
        <div class="ui container">
            <a href="{url('/')}" class="header item" data-pjax="true">
                <img class="logo" src="{$assets}/images/logo-mini.png" style="height:36px;" />
                東木书屋
            </a>

            <a class="item" href="{url('/rank')}" data-pjax="">
                <i class="icon-paihang"></i> 排行
            </a>
            <a class="item" href="{url('/shelf')}" data-pjax="true">
                <i class="icon-shelf"></i>
                书架
            </a>
            <div class="right menu">
                <div class="item">
                    <form class="form" action="{url('/search')}" method="post" id="searchForm">
                        <div class="ui mini icon input search">
                            <input type="text" name="key" placeholder="搜索小说/作者" class="prompt" style="width:200px;">
                            <i class="inverted circular search link icon"></i>
                            <div class="results"></div>
                        </div>
                    </form>
                </div>
                {if isset($user)}
                <div class="ui simple dropdown item">
                    <img class="ui avatar image" src=" {$assets}/images/avatar/<?=$user['avatar'] ?: 'default.jpg'?>">
                    <span>{$user['username']}</span>
                    <div class="menu">
                        <a class="item" href="{url('/user/config')}" data-pjax="true">
                            <i class="icon-profile"></i>
                            个人属性
                        </a>

                        <div class="divider"></div>
                        {if $user['group']['id'] == 1}
                        <a class="item" href="{url('/admin')}">
                            <i class="cog icon"></i>
                            后台管理
                        </a>
                        {/if}
                        <a class="item" href="{url('/user/logout')}">
                            <i class="power off icon"></i>
                            退出登录
                        </a>
                    </div>
                </div>
                {else}
                <a class="item" href="{url('/user')}" data-pjax="true">
                    <i class="user icon"></i> 登录
                </a> {/if}
            </div>
        </div>
    </div> <!-- END Menu content -->

    <!-- main content -->
    <div class="ui container" id="mainContent">
        {$content}
    </div>
    <!--END main content -->

    <div class="ui vertical footer segment">
        <div class="ui center aligned container">
            <div class="ui section divider"></div>
            <img src="{$assets}/images/logo.png" class="ui centered small image">
            <div class="ui horizontal small divided link list">
                <a class="item" href="http://semantic-ui.mit-license.org/" target="_blank">Free &amp; Open Source
                    (MIT)</a>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{$assets}/js/include.js"></script>
    <script type="text/javascript" src="{$assets}/js/jquery.pjax.js"></script>
    <script type="text/javascript" src="{$assets}/js/nprogress.js"></script>
    <script type="text/javascript" src="{$assets}/js/semantic.min.js"></script>
    <script type="text/javascript" src="{$assets}/js/jquery.toast.min.js"></script>
    <script type="text/javascript" src="{$assets}/js/function.js"></script>
    <script type="text/javascript">
    $(function() {
        $("i.search.icon").click(function() {
            $('#searchForm').submit();
        });

        $('#searchForm').submit(function() {
            var key = $("input[name=key]").val();
            if (key == "") {
                alert('请输入搜索的关键字');
                return false;
            }
        });

        $('.ui.search').search({
            apiSettings: {
                url: '{url("/search/key")}?q={query}'
            },
            fields: {
                results: 'items',
                title: 'title'
            },
            minCharacters: 0,
            onSelect: function(result, response) {
                window.location.href = '{url("/search")}?key=' + result.title
            }
        });
    });
    </script>

</body>

</html>