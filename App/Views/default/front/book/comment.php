<div class="comment">
    <a class="avatar">
        <img src="{$assets}/images/avatar/<?= $comment['avatar'] ?: 'default.jpg' ?>">
    </a>
    <div class="content">
        <a class="author">{$comment['username']}</a>
        <div class="metadata">
            <span class="date"><?= date("Y-m-d H:m", $comment['date']) ?></span>
        </div>
        <div class="text">
            <?= $comment['content'] = preg_replace("/:([a-z\-\_]+):/i", "<i class='em em-$1'></i>", $comment['content']); ?>
        </div>
        <div class="actions">
            <a class="reply">
                <i class="comment outline icon"></i>
                回复</a>
        </div>
    </div>
</div>