<!DOCTYPE html>
<html>

<head>
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <title><?= isset($viewTitle) ? $viewTitle . " - " : ""; ?>{$title}</title>
    <link rel="stylesheet" type="text/css" href="{$assets}/css/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="{$assets}/css/read.css">
    <link rel="stylesheet" type="text/css" href="{$assets}/css/iconfont.css">
    <link rel="stylesheet" type="text/css" href="{$assets}/css/jquery.toast.min.css">
    <script type="text/javascript" src="{$assets}/js/jquery.min.js"></script>
</head>

<body>
    <!-- Sidebar -->
    <div class="ui chapterList vertical menu">
        {foreach $chapters as $c}
        <a class="item <?= ($c['id'] == $chapter['id']) ? 'active' : '' ?>"
            href="{url('/read/')}{$story['id']}/{$c['id']}" data-pjax="" data-chapter-id="{$c['id']}">
            {$c['title']}
        </a>
        {/foreach}
    </div>
    <!-- End Sidebar -->

    <!-- Menu content -->
    <div class="ui teal fixed  borderless inverted  menu">
        <div class="ui container">
            <a href="{url('/')}" class="header item" data-pjax="true">
                <img class="logo" src="{$assets}/images/logo-mini.png" />
            </a>
            <div class="item">
                <div class="ui breadcrumb">
                    <a class="section" href="{url('/')}">首页</a>
                    <i class="right angle icon divider"></i>
                    <a class="section" href="{url('/book/')}{$story['id']}">{$story['title']}</a>
                    <i class="right angle icon divider"></i>
                    <div class="active section">{$chapter['title']}</div>
                </div>
            </div>
            <div class="right menu">
                <a class="item" href="{url('/rank')}" data-pjax="true">
                    <i class="icon-paihang"></i>
                </a>
                <a class="item" href="{url('/shelf')}" data-pjax="true">
                    <i class="icon-shelf"></i>
                </a>
                <a class="item" id="chapterList">
                    <i class="list icon"></i>
                </a>
                <a class="item" id="config">
                    <i class="cog icon"></i>
                </a>
                {if isset($user)}
                <div class="ui simple dropdown item">
                    <img class="ui avatar image" src="<?= $user['avatar'] ?: $assets . '/images/avatar/default.jpg' ?>">
                    <div class="menu">
                        <a class="item" href="{url('/user/config')}" data-pjax="true">
                            <i class="user icon"></i>
                            个人属性
                        </a>

                        <div class="divider"></div>
                        {if $user['group']['id'] == 1}
                        <a class="item" href="{url('/admin')}">
                            <i class="cog icon"></i>
                            后台管理
                        </a>
                        {/if}
                        <a class="item" href="{url('/user/logout')}">
                            <i class="power off icon"></i>
                            退出登录
                        </a>
                    </div>
                </div>
                {else}
                <a class="item" href="{url('/user')}" data-pjax="true">
                    <i class="user icon"></i> 登录
                </a> {/if}
            </div>
        </div>
    </div> <!-- END Menu content -->

    <!-- main content -->

    <div class="ui container" style="width:980px">
        <div class="ui message">
            <div class="content" id="content">
                <?= $chapter['content'] = "<p>" . preg_replace("/\n/", '</p><p>',$chapter['content']) . "</p>" ?>
            </div>
            <div class="pagination" id="pagination" url="{url('/read/'.$story['id'])}">
                <dd id="prev" data-chapter="<?= $chapter['id'] > 1 ? $chapter['id'] - 1 : -1; ?>"></dd>
                <dd id="cent" data-chapter="{$chapter['id']}"></dd>
                <dd id="next" data-chapter="<?= ($chapter['id'] == count($chapters)) ? 0 : $chapter['id'] + 1; ?>"></dd>
            </div>
            <div id="cPage"></div>
        </div>
    </div>
    <!--END main content -->


    <!-- Popup -->
    <div class="ui special popup">
        <div class="header" style="min-width:350px;margin:10px 0;font-size:18px">设置</div>
        <div class="ui grid">
            <div class="four wide column right">背景色</div>
            <div class="twelve wide column" id="setBackground">
                <i class="big icons">
                    <i class=" circle icon" style="color:#FAF9DE;"> </i>
                </i>
                <i class="big icons">
                    <i class=" circle icon" style="color:#FFF2E2;"> </i>
                </i>
                <i class="big icons">
                    <i class=" circle icon" style="color:#FDE6E0;"> </i>
                </i>
                <i class="big icons">
                    <i class=" circle icon" style="color:#E3EDCD;"> </i>
                </i>
                <i class="big icons">
                    <i class=" circle icon" style="color:#EAEAEF;"> </i>
                </i>
            </div>

            <div class="four wide column right">字体尺寸</div>
            <div class="twelve wide column">
                <div class="ui basic buttons" id="setFontSize">

                    <div class="ui button minus">
                        <i class="medium icons">
                            <i class="font icon"></i>
                            <i class="top right corner minus icon"></i>
                        </i>
                    </div>
                    <div class="ui teal button" id="fontSize">18</div>
                    <div class="ui button add">
                        <i class="medium icons">
                            <i class="font icon"></i>
                            <i class="top right corner add icon"></i>
                        </i>
                    </div>

                </div>
            </div>

            <div class="four wide column right">行间距</div>
            <div class="twelve wide column">
                <div class="ui basic buttons" id="setLineHeight">

                    <div class="ui button minus">
                        <i class="medium icons">
                            <i class="align justify icon"></i>
                            <i class="top right corner minus icon"></i>
                        </i>
                    </div>
                    <div class="ui teal button" id="lineHeight">1.5</div>
                    <div class="ui button add">
                        <i class="medium icons">
                            <i class="align justify icon"></i>
                            <i class="top right corner add icon"></i>
                        </i>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{$assets}/js/include.js"></script>
    <script type="text/javascript" src="{$assets}/js/jquery.pjax.js"></script>
    <script type="text/javascript" src="{$assets}/js/nprogress.js"></script>
    <script type="text/javascript" src="{$assets}/js/semantic.min.js"></script>
    <script type="text/javascript" src="{$assets}/js/jquery.toast.min.js"></script>
    <script type="text/javascript" src="{$assets}/js/function.js"></script>
    <script type="text/javascript" src="{$assets}/js/read.function.js"></script>
    <script type="text/javascript" src="{$assets}/js/MyPagination.js"></script>
    <script type="text/javascript">
    $(function() {
        $("#prev,#next").click(function() {
            var chapter = $(this).data("chapter");
            if (chapter > 0) {
                window.location.href = "{url('/read/')}{$story['id']}/" + chapter;
            } else if (chapter == -1) {
                $.toast({
                    position: 'top-center',
                    text: "再往前没有了...",
                    icon: "warning"
                });
            } else {
                $.toast({
                    position: 'top-center',
                    text: "再往后没有了...",
                    icon: "warning"
                });
            }
        });
        setChapterListHeight();
        var scrollTop = parseInt($(".item[data-chapter-id={$chapter['id']}]").offset().top);
        $(".chapterList").data("show", false).scrollTop(scrollTop > 41 ? scrollTop - 41 : 0);

    });
    </script>

</body>

</html>