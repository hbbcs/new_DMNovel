<div class="ui breadcrumb">
    <a class="section" href="{url('/')}" data-pjax="">首页</a>
    <i class="right angle icon divider"></i>
    <a class="section" href="{url('/category/')}{$category['id']}" data-pjax="">{$story['category']}</a>
    <i class="right angle icon divider"></i>
    <a class="section" href="{url('/category/')}{$subcategory['id']}" data-pjax="">{$story['sub_category']}</a>
    <i class="right angle icon divider"></i>
    <div class="active section">{$viewTitle}</div>
</div>


<div class="ui message items">
    <div class="item">
        <div class="image">
            <img src="{$assets}/images/covers/{$story['cover']}">
        </div>
        <div class="content">
            <a class="header">
                <h2>{$story['title']}</h2>
            </a>


            <div class="description">
                <div class="ui labeled mini button" tabindex="0" title="作者">
                    <div class="ui mini icon button">
                        <i class="icon user"></i>
                    </div>
                    <a class="ui basic left pointing label" href="{url('/search')}?key={$story['author']}"
                        data-pjax=true>
                        {$story['author']}
                    </a>
                </div>
            </div>
            <div class="description">
                <p>{$story['desc']}</p>
            </div>
            <div class="extra">
                {if $story['last_chapter']}
                <p>
                    最后章节：{$story['last_chapter']}
                    (<?=date("m月d日 H:i 更新", $story['last_update'])?>)
                </p>
                {/if}
                <div class="ui  labeled button " id="shelf">
                    <div class="ui toggle button <?=$favorites ? "yellow" : ""?>">
                        <i class="heart icon"></i> 收藏
                    </div>
                    <a class="ui basic left pointing label">
                        {$story['favorites']}
                    </a>
                </div>
                <div class="ui toggle labeled button" id="score">
                    <div class="ui toggle button">
                        <i class="star icon"></i> 投票
                    </div>
                    <a class="ui basic left pointing  label">
                        {$story['vote']}
                    </a>
                </div>
                <div class="ui toggle labeled button">
                    <div class="ui toggle button">
                        <i class="check circle outline icon"></i> 点击
                    </div>
                    <a class="ui basic left pointing  label">
                        {$story['click']}
                    </a>
                </div>
                <div class="ui simple blue dropdown item button" id="download">
                    <i class="download icon"></i> 下载
                    <div class="menu">
                        <div class="item" data-type="txt">Txt 格式</div>
                        <div class="item" data-type="epub">Epub格式</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ui pointing secondary menu">
    <a class="item active" data-tab="comment">评论</a>
    <a class="item" data-tab="chapters">章节</a>
    <div class="right menu">
        <a class="ui item" id="sortChapter" data-sort="1" title="正序"><i class="icon sort amount up"></i></a>
    </div>
</div>

<div class="ui tab segment active" style="" data-tab="comment" id="commentContent">
    <div class="ui comments">
        {foreach $comments as $comment}
        <div class="comment">
            <a class="avatar">
                <img src="{$assets}/images/avatar/<?=$comment['avatar'] ?: 'default.jpg'?>">
            </a>
            <div class="content">
                <a class="author">{$comment['username']}</a>
                <div class="metadata">
                    <span class="date"><?=date("Y-m-d H:m", $comment['date'])?></span>
                </div>
                <div class="text">
                    <?=$comment['content'] = preg_replace("/:([a-z\-\_]+):/i", "<i class='em em-$1'></i>", $comment['content']);?>
                </div>
                <div class="actions">
                    <a class="reply">
                        <i class="comment outline icon"></i>
                        回复</a>
                </div>
            </div>
        </div>
        {/foreach}
    </div>
    <form class="ui reply form" action="{url('/comment/post')}" method="post" id="commentForm">
        <div class="field">
            <textarea name="content" placeholder="评论内容，最多100字符"></textarea>
            <input type="hidden" name="id" value="{$story['id']}" />
        </div>
        <a class="ui label" id="expression">
            <i class="smile outline icon"></i> 插入表情
        </a>
        <span id="textCount">字数统计：<i>0</i></span>
        <div class="ui blue labeled submit icon button right floated">
            <i class="icon edit"></i> 发表评论
        </div>

    </form>
</div>
<div class="ui tab segment" data-tab="chapters" style="min-height:300px;">
    <div class="ui active inverted dimmer">
        <div class="ui text loader">正在从网络下载章节<i></i>，请稍候...</div>
    </div>
    <p></p>
</div>

<!-- Vote Popup -->
<div class="ui special popup vote">
    <div class="header" style="min-width:150px;font-size:18px">投票</div>
    <div class="content">一次最多投5票，票数是累计的</div>
    <div class="ui star rating" data-rating="3"></div>
</div>
<!-- end vote -->
<!-- Popup -->
<div class="ui special popup expression">
    <div class="content">
        <i class="em em-smile" data-em="smile"></i>
        <i class="em em-laughing" data-em="laughing"></i>
        <i class="em em-blush" data-em="blush"></i>
        <i class="em em-smiley" data-em="smiley"></i>
        <i class="em em-relaxed" data-em="relaxed"></i>
        <i class="em em-smirk" data-em="smirk"></i>
        <i class="em em-heart_eyes" data-em="heart_eyes"></i>
        <i class="em em-kissing_heart" data-em="kissing_heart"></i>
        <i class="em em-kissing_closed_eyes" data-em="kissing_closed_eyes"></i>
        <i class="em em-flushed" data-em="flushed"></i>
        <i class="em em-relieved" data-em="relieved"></i>
        <i class="em em-satisfied" data-em="satisfied"></i>
        <i class="em em-grin" data-em="grin"></i>
        <i class="em em-wink" data-em="wink"></i>
        <i class="em em-stuck_out_tongue_winking_eye" data-em="stuck_out_tongue_winking_eye"></i>
        <i class="em em-stuck_out_tongue_closed_eyes" data-em="stuck_out_tongue_closed_eyes"></i>
        <i class="em em-grinning" data-em="grinning"></i>
        <i class="em em-kissing" data-em="kissing"></i>
        <i class="em em-kissing_smiling_eyes" data-em="kissing_smiling_eyes"></i>
        <i class="em em-stuck_out_tongue" data-em="stuck_out_tongue"></i>
        <i class="em em-sleeping" data-em="sleeping"></i>
        <i class="em em-worried" data-em="worried"></i>
        <i class="em em-frowning" data-em="frowning"></i>
        <i class="em em-anguished" data-em="anguished"></i>
        <i class="em em-open_mouth" data-em="open_mouth"></i>
        <i class="em em-grimacing" data-em="grimacing"></i>
        <i class="em em-confused" data-em="confused"></i>
        <i class="em em-hushed" data-em="hushed"></i>
        <i class="em em-expressionless" data-em="expressionless"></i>
        <i class="em em-unamused" data-em="unamused"></i>
        <i class="em em-sweat_smile" data-em="sweat_smile"></i>
        <i class="em em-sweat" data-em="sweat"></i>
        <i class="em em-disappointed_relieved" data-em="disappointed_relieved"></i>
        <i class="em em-weary" data-em="weary"></i>
        <i class="em em-pensive" data-em="pensive"></i>
        <i class="em em-disappointed" data-em="disappointed"></i>
        <i class="em em-confounded" data-em="confounded"></i>
        <i class="em em-fearful" data-em="fearful"></i>
        <i class="em em-cold_sweat" data-em="cold_sweat"></i>
        <i class="em em-persevere" data-em="persevere"></i>
        <i class="em em-cry" data-em="cry"></i>
        <i class="em em-sob" data-em="sob"></i>
        <i class="em em-joy" data-em="joy"></i>
        <i class="em em-astonished" data-em="astonished"></i>
        <i class="em em-scream" data-em="scream"></i>
        <i class="em em-tired_face" data-em="tired_face"></i>
        <i class="em em-angry" data-em="angry"></i>
        <i class="em em-rage" data-em="rage"></i>
        <i class="em em-triumph" data-em="triumph"></i>
        <i class="em em-sleepy" data-em="sleepy"></i>
        <i class="em em-yum" data-em="yum"></i>
        <i class="em em-mask" data-em="mask"></i>
        <i class="em em-sunglasses" data-em="sunglasses"></i>
        <i class="em em-dizzy_face" data-em="dizzy_face"></i>
        <i class="em em-imp" data-em="imp"></i>
        <i class="em em-smiling_imp" data-em="smiling_imp"></i>
        <i class="em em-neutral_face" data-em="neutral_face"></i>
        <i class="em em-no_mouth" data-em="no_mouth"></i>
        <i class="em em-innocent" data-em="innocent"></i>
        <i class="em em-alien" data-em="alien"></i>
        <i class="em em-yellow_heart" data-em="yellow_heart"></i>
        <i class="em em-blue_heart" data-em="blue_heart"></i>
        <i class="em em-purple_heart" data-em="purple_heart"></i>
        <i class="em em-heart" data-em="heart"></i>
        <i class="em em-green_heart" data-em="green_heart"></i>
        <i class="em em-broken_heart" data-em="broken_heart"></i>
        <i class="em em-heartbeat" data-em="heartbeat"></i>
        <i class="em em-heartpulse" data-em="heartpulse"></i>
        <i class="em em-two_hearts" data-em="two_hearts"></i>
        <i class="em em-revolving_hearts" data-em="revolving_hearts"></i>
        <i class="em em-cupid" data-em="cupid"></i>
        <i class="em em-sparkling_heart" data-em="sparkling_heart"></i>
        <i class="em em-sparkles" data-em="sparkles"></i>
        <i class="em em-star" data-em="star"></i>
        <i class="em em-star2" data-em="star2"></i>
        <i class="em em-dizzy" data-em="dizzy"></i>
        <i class="em em-boom" data-em="boom"></i>
        <i class="em em-collision" data-em="collision"></i>
        <i class="em em-anger" data-em="anger"></i>
        <i class="em em-exclamation" data-em="exclamation"></i>
        <i class="em em-question" data-em="question"></i>
        <i class="em em-grey_exclamation" data-em="grey_exclamation"></i>
        <i class="em em-grey_question" data-em="grey_question"></i>
        <i class="em em-zzz" data-em="zzz"></i>
        <i class="em em-dash" data-em="dash"></i>
        <i class="em em-sweat_drops" data-em="sweat_drops"></i>
        <i class="em em-notes" data-em="notes"></i>
        <i class="em em-musical_note" data-em="musical_note"></i>
        <i class="em em-fire" data-em="fire"></i>
        <i class="em em-hankey" data-em="hankey"></i> </div>
</div>
<script type="text/javascript">
$(function() {
    $.include.set("{$assets}/").file(["apple.css", "jquery.validator.js"]);
    $("div[data-tab=chapters]").load("{url('/book/chapters/')}{$story['id']}/0");

    $('.secondary.menu .item').tab();
    $("#score").popup({
        popup: '.special.popup.vote',
        on: 'click',
        position: "bottom left"
    });

    $("#expression").popup({
        popup: '.special.popup.expression',
        on: 'click',
        position: "top left"
    })

    $(".expression i").click(function() {
        var emoji = $(this).data("em");
        $("[name=content]").append(":" + emoji + ":");
        $("#expression").popup("hide");
    });

    $(".submit.button").click(function() {
        $(this).parents("form").submit();
    })

    $('#commentForm').validator({
        success: function(data) {
            $.toast({
                heading: data.status,
                position: 'top-center',
                text: data.message,
                icon: data.status == "Error" ? "warning" : "success"
            });
            if (data.status != "Error") {
                $("#commentContent .comments").prepend(data.html);
            }
        }
    });

    $('.ui.popup .rating').rating({
        initialRating: 1,
        maxRating: 5,
        onRate: function(value) {
            $.post("{url('/vote')}", {
                story: "{$story['id']}",
                value: value
            }, function(data) {
                data = $.parseJSON(data);
                $.toast({
                    heading: data.status,
                    position: 'top-center',
                    text: data.message,
                    icon: data.status == "Error" ? "warning" : "success"
                });
            })
        }
    });

    $("#download .item").click(function() {
        var type = $(this).data("type");
        $.post("{url('/download/')}", {
            'id': "{$story['id']}",
            'type': type
        }, function(data) {
            var e = $.parseJSON(data);
            if (e.message)
                alert(e.message, e.status);
            if (e.status != "error") {
                alert("文件已生成，请下载...");
                window.location.href = "{url('/download/get/')}" + e.path + "/" + e.name;
            }
        });
    });

    $("#shelf").click(function() {
        var favo = <?=$favorites ? "true" : "false"?>;
        var num = <?=$story['favorites']?>;
        var $this = $(this);
        $.get("{url('/shelf/favorites/')}{$story['id']}", function(data) {
            data = $.parseJSON(data);
            alert(data.message, data.status);
            if (data.status != "error") {
                if (favo) {
                    $this.children(".button").removeClass("yellow");
                    $this.children("a").html(num - 1);
                } else {
                    $this.children(".button").addClass("yellow");
                    $this.children("a").html(num + 1);
                }
            }
        })
    });

    $("textarea[name=content]").on('input propertychange', function() {
        var content = $(this).val();
        $("#textCount").children("i").html(content.length)
        if (content.length > 100) {
            alert("内容长度最多100字符...");
            $(this).val(content.substr(0, 100));
        }
    });

    $("#sortChapter").click(function() {
        var sort = $(this).data("sort");
        $("div[data-tab=chapters]").load("{url('/book/chapters/')}{$story['id']}/" + sort);
        if (sort==1) {
            $(this).find("i").removeClass("up").addClass("down");
        } else {
            $(this).find("i").removeClass("down").addClass("up");
        }
        $(this).data("sort", sort == 0 ? 1 : 0).attr("title", sort == 0 ? "正序" : "倒序");
    })

});
</script>