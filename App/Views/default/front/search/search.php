<div class="ui breadcrumb">
    <a class="section" href="{url('/')}" data-pjax="">首页</a>
    <i class="right chevron icon divider"></i>
    <div class="active section">搜索: <a href="{url('/search')}?key={$key}" data-pjax="">{$key}</a></div>
</div>

<div class="ui secondary pointing menu " id="searchMenu">
    <a class="item active" data-type="local">
        <i class="icon server"></i>
        本地
    </a>
    <a class="item" data-type="cloud">
        <i class="icon cloud"></i>
        追书
    </a>
</div>

{foreach $searchKeys as $skey}
<a class="ui red tag label">{$skey['title']}</a>
{/foreach}

<div class="ui segments">
    <div class="ui secondary segment">
        <label><strong id="searchLocal">本地</strong></label>搜索关键字-<strong>{$key}</strong>,共有<strong id="searchNum">{$stories['total']}</strong>部小说
    </div>
    <div class="ui segment">
        <div class="ui relaxed divided list" id="searchContent" data-page="{$page}" data-type="{$type}">
            {foreach $stories['books'] as $story}
            <div class="item story">
                <img src="{$assets}/images/covers/default.jpg" data-src="<?= ($type == 'local') ? $assets . '/images/covers/' : $staticUrl ?>{$story['cover']}" class="miniCover" />
                <div class="content">
                    <a class="header <?= $type == 'local' ?: 'cloud' ?>" href="{url('/book/')}<?= isset($story['id']) ? $story['id'] : $story['_id'] ?>/<?= $type == 'local' ?: 'cloud' ?>" data-pjax="">
                        <h3>{$story['title']}</h3>
                    </a>
                    <div class="description">
                        <div class="ui labeled mini button" tabindex="0" title="作者">
                            <div class="ui mini icon button">
                                <i class="icon user"></i>
                            </div>
                            <a class="ui basic left pointing label" href="{url('/search')}?key={$story['author']}" data-pjax=true>
                                {$story['author']}
                            </a>
                        </div>
                        {if $type=="cloud"}
                        <div class="ui labeled mini button" tabindex="0" title="留存率">
                            <div class="ui mini icon button">
                                <i class="icon heart"></i>
                            </div>
                            <a class="ui basic left pointing label">
                                {$story['retentionRatio']}
                            </a>
                        </div>
                        <div class="ui labeled mini button" tabindex="0" title="追书者">
                            <div class="ui mini icon button">
                                <i class="icon flag"></i>
                            </div>
                            <a class="ui basic left pointing label">
                                {$story['latelyFollower']}
                            </a>
                        </div>
                        {/if}
                    </div>
                    <div class="description">
                        简介：<?= isset($story['shortIntro']) ? $story['shortIntro'] : $story['desc'] ?>
                    </div>
                </div>
            </div>
            {/foreach}
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('#searchMenu a.item').click(function(e) {
            $('#searchMenu').find(".active").removeClass('active');
            $(this).addClass("active");
            $('#searchContent')
                .data("type", $(this).data('type'))
                .data("page", 0)
                .addClass("loading");
            e.preventDefault();
            $.post("{url('/search/')}", {
                'type': $(this).data('type'),
                'key': "{$key}"
            }, function(data) {
                $('#searchContent').html(data).removeClass("loading");
            });
        });

        $(".ui.tag.label").click(function() {
            var key = $(this).text();
            $("body").addClass("loading");
            window.location.href = "{url('/search')}" + "?key=" + key
        });

        $(document).on("click", "a.cloud", function() {
            $.toast({
                position: 'top-center',
                text: "正在加载小说，请稍候...",
                icon: "info",
                hideAfter: false
            });
        })

        $(document).scroll(function() {
            $(".miniCover[data-isLoaded!=1]").imageLoad();
            var htmlHeight = document.body.scrollHeight || document.documentElement.scrollHeight;
            //clientHeight是网页在浏览器中的可视高度，
            var clientHeight = document.body.clientHeight || document.documentElement.clientHeight;
            //scrollTop是浏览器滚动条的top位置，
            var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
            var allPage = Math.ceil(parseInt($("#searchNum").text()) / 20)
            var page = parseInt($('#searchContent').data("page")) + 1;
            var message;
            //通过判断滚动条的top位置与可视网页之和与整个网页的高度是否相等来决定是否加载内容；
            if (scrollTop + clientHeight == htmlHeight) {
                if (allPage > page) {
                    message = "正在加载，请稍候..."
                    var type = $('#searchContent').data("type");
                    $.get("{url('/search')}?key={$key}&type=" + type + "&page=" + page, function(data) {
                        $('#searchContent').data("page", page).append(data);
                    });
                } else {
                    message = "已全部加载完毕";
                }
                alert(message, "success");
            }
        });
    });
</script>