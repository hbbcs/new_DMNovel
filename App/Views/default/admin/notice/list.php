<div class="ui grid">
    <div class="two column row">
        <div class="column">
            <div class="ui breadcrumb">
                <a class="section" href="{url('/admin')}">管理首页</a>
                <i class="right angle icon divider"></i>
                <a class="section active">{$viewTitle}</a>
            </div>
        </div>
        <div class="column right aligned">
            <div class="ui mini basic icon buttons">
                <a class="ui button" title="发布公告" data-position="bottom center"
                    data-dialog="{url('/admin/notice/add')}">
                    <i class="icons">
                        <i class="bullhorn icon"></i>
                        <i class="top right corner add icon"></i>
                    </i>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="ui divider"></div>

<div class="ui segment">
    <div class="ui divided items" id="noticeItems">
        {foreach $notices as $notice}
        <div class="item" data-noticeid="{$notice['id']}">
            <div class="middle aligned content">
                <div class="media">
                    <?=date("Y/m/d H:i",$notice['date'])?>
                    <a class="ui right floated deleteNoticeBtn ">
                        <i class="icon trash circular small inverted teal"></i>
                    </a>
                    <a class="ui right floated " data-dialog="{url('/admin/notice/add/')}/{$notice['id']}">
                        <i class="icon circular inverted teal small edit"></i>
                    </a>
                </div>
                <div class="description">
                    <p>{$notice['content']}</p>
                </div>
                <div class="extra">

                </div>
            </div>
        </div>
        {/foreach}
    </div>
</div>

<div class="ui pages right aligned segment"></div>

<script type="text/javascript">
$(function() {
    $.include.set("{$assets}/").file(['jquery.pagination.js']);
    $(".pages").pagination({
        total: <?=$total?>,
        'url': '{url("/admin/notice")}',
        'ajax': 'post',
        "target": '#noticeItems'
    })

    $(document).on("click", ".deleteNoticeBtn", function() {
        var item = $(this).parents(".item")
        var id = item.data("noticeid");
        confirm({
            title: "警告",
            content: "确认删除此公告?",
            'okBtn': {
                class: "green",
                name: "确认",
                action: function() {
                    $.get("{url('/admin/notice/delete/')}" + id, function(data) {
                        data = $.parseJSON(data);
                        alert(data.message, data.status == "Error" ? "warning" :
                            "success");
                        if (data.status != "Error")
                            item.remove();
                    });
                }
            }
        });
    });

});
</script>