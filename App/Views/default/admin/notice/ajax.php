{foreach $notices as $notice}
<div class="item" data-id="{$notice['id']}">
    <div class="middle aligned content">
        <div class="media">
            <?=date("Y/m/d H:i",$notice['date'])?>
            <a class="ui right floated ">
                <i class="icon trash circular small inverted teal"></i>
            </a>
            <a class="ui right floated " data-dialog="{url('/admin/notice/add/')}/{$notice['id']}">
                <i class="icon circular inverted teal small edit"></i>
            </a>
        </div>
        <div class="description">
            <p>{$notice['content']}</p>
        </div>
        <div class="extra">

        </div>
    </div>
</div>
{/foreach}