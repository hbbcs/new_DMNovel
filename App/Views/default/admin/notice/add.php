<form class="ui reply form" action="{url('/admin/notice/save')}" method="post" id="noticeForm">
    <div class="field">
        <textarea name="content" placeholder="公告内容，最多100字符"><?=isset($notice)?$notice['content']:''?></textarea>
        <input type="hidden" name="id" value="<?=isset($notice)?$notice['id']:''?>" />
    </div>
    <span id="textCount">字数统计：<i><?=isset($notice)?strlen($notice['content']):0?></i></span>
    <button class="ui blue labeled submit icon button right floated" type="submit">
        <i class="icon edit"></i> 发表公告
    </button>
</form>

<script type="text/javascript">
$(function() {
    $.include.set("{$assets}/").file(['jquery.validator.js']);
    $("textarea[name=content]").on('input propertychange', function() {
        var content = $(this).val();
        if (content.length > 100) {
            alert("内容长度最多100字符...");
            $(this).val(content.substr(0, 100));
        } else {
            $("#textCount").children("i").html(content.length)
        }
    });

    $('#noticeForm').validator({
        success: function(data) {
            alert(data.message, data.status == "Error" ? "warning" : "success");
            if (data.status != "Error") {
                dialog.close("modal_ajax");
                $(".pages").pagination.refresh("1")
            }
        }
    });
});
</script>