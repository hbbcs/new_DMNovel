<!DOCTYPE html>
<html>

<head>
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <title><?= isset($viewTitle) ? $viewTitle . " - " : ""; ?>{$title}</title>
    <link rel="stylesheet" type="text/css" href="{$assets}/css/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="{$assets}/css/main.css">
    <link rel="stylesheet" type="text/css" href="{$assets}/css/iconfont.css">
    <link rel="stylesheet" type="text/css" href="{$assets}/css/nprogress.css">
    <link rel="stylesheet" type="text/css" href="{$assets}/css/jquery.toast.min.css">
    <script type="text/javascript" src="{$assets}/js/jquery.min.js"></script>
</head>

<body>
    <!-- Menu content -->
    <div class="ui blue fixed pointing borderless inverted menu">
        <div class="ui container">
            <a href="{url('/')}" class="header item" data-pjax="true">
                <img class="logo" src="{$assets}/images/logo-mini.png" style="height:36px;" />
                東木书屋
            </a>

            <a class="item" href="{url('/admin')}" data-pjax="">
                <i class="icon home"></i> 管理首页
            </a>
            <a class="item" href="{url('/admin/story')}" data-pjax="">
                <i class="icon book"></i>
                小说管理
            </a>
            <a class="item" href="{url('/admin/users')}" data-pjax="">
                <i class="icon users"></i>
                用户管理
            </a>
            <a class="item" href="{url('/admin/notice')}" data-pjax="">
                <i class="icon bullhorn"></i>
                公告管理
            </a>
            <div class="right menu">
                <div class="ui simple dropdown item">
                    <img class="ui avatar image" src="<?= $user['avatar'] ?: $assets . '/images/avatar/default.jpg' ?>">
                    <span>{$user['username']}</span>
                    <div class="menu">
                        <a class="item" href="{url('/user/config')}" data-pjax="true">
                            <i class="icon-profile"></i>
                            个人属性
                        </a>

                        <div class="divider"></div>
                        {if $user['group']['id'] == 1}
                        <a class="item" href="{url('/admin')}">
                            <i class="cog icon"></i>
                            后台管理
                        </a>
                        {/if}
                        <a class="item" href="{url('/user/logout')}">
                            <i class="power off icon"></i>
                            退出登录
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END Menu content -->

    <!-- main content -->
    <div class="ui container" id="mainContent">
        {$content}
    </div>
    <!--END main content -->

    <div class="ui vertical footer segment">
        <div class="ui center aligned container">
            <div class="ui section divider"></div>
            <img src="{$assets}/images/logo.png" class="ui centered small image">
            <div class="ui horizontal small divided link list">
                <a class="item" href="http://semantic-ui.mit-license.org/" target="_blank">Free &amp; Open Source
                    (MIT)</a>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{$assets}/js/include.js"></script>
    <script type="text/javascript" src="{$assets}/js/jquery.pjax.js"></script>
    <script type="text/javascript" src="{$assets}/js/nprogress.js"></script>
    <script type="text/javascript" src="{$assets}/js/semantic.min.js"></script>
    <script type="text/javascript" src="{$assets}/js/jquery.toast.min.js"></script>
    <script type="text/javascript" src="{$assets}/js/function.js"></script>

</body>

</html>