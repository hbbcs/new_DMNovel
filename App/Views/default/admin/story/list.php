<div class="ui grid">
    <div class="two column row">
        <div class="column">
            <div class="ui breadcrumb">
                <a class="section" href="{url('/admin')}">管理首页</a>
                <i class="right angle icon divider"></i>
                <a class="section active">{$viewTitle}</a>
            </div>
        </div>
        <div class="column right aligned">
            <div class="ui mini basic icon buttons">
                <a class="ui button" title="新建小说" data-tooltip="新建小说" data-position="bottom center"
                    href="{url('/admin/story/add')}">
                    <i class="icons">
                        <i class="book icon"></i>
                        <i class="top right corner add icon"></i>
                    </i>
                </a>
                <a class="ui button" data-tooltip="上传小说" data-position="bottom center"
                    data-dialog="{url('/admin/story/upload')}">
                    <i class="upload icon"></i>
                </a>
                <a class="ui button" data-tooltip="更新所有网络小说" data-position="bottom right" id="updateAll">
                    <i class="download icon"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="ui divider"></div>

<table class="ui red striped selectable single line table" id="storyTable">
    <thead>
        <tr>
            <th>名称</th>
            <th>作者</th>
            <th>加入时间</th>
            <th>最后更新时间</th>
            <th>最后更新章节</th>
            <th width="100">操作</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<script type="text/javascript">
$(function() {
    $.include.set("{$assets}/").file(["datatables.min.css", "datatables.min.js"]);

    var storyTable = $("#storyTable").DataTable({
        'language': {
            "url": "<?= $assets ?>/js/datatable.chinese.json"
        },
        'stateSave': false,
        "processing": true,
        "serverSide": true,
        "searching": true,
        'ajax': "{url('/admin/story/table')}",
        "order": [
            [3, "desc"]
        ],
        columnDefs: [{
            targets: 0,
            render: function(a, b, c, d) {
                var html = "<a target=\"_blank\" href=\"{url('/book')}/" + c.id + "\">《" + c
                    .title +
                    "》</a>";
                return html;
            }
        }, {
            targets: 1,
            render: function(a, b, c, d) {
                var html = "<a target=\"_blank\" href=\"{url('/search')}?key=" + c.author +
                    "\">" + c.author +
                    "</a>";
                return html;
            }
        }, {
            targets: 2,
            render: function(a, b, c, d) {
                return new Date(parseInt(c.time) * 1000).toLocaleString();
            }
        }, {
            targets: 3,
            render: function(a, b, c, d) {
                return c.last_update ? new Date(parseInt(c.last_update) * 1000)
                    .toLocaleString() : "从未更新";
            }
        }, {
            targets: 4,
            orderable: false,
            render: function(a, b, c, d) {
                var html = "<a target=\"_blank\" href=\"{url('/read/')}" + c.id + "/" + c
                    .last_chapter_id +
                    "\">" + c.last_chapter.substring(0, 32) + "</a>";
                return html;
            }
        }, {
            targets: 5,
            orderable: false,
            render: function(a, b, c, d) {
                var downloadBtn = (c.type == "cloud") ?
                    '<button class="ui button updateStoryBtn" data-tooltip="更新网络小说" data-position="bottom right"><i class="download icon"></i></button>' :
                    '';
                var html = '<div class="ui mini basic icon buttons" data-storyid="' + c.id +
                    '">' +
                    ' <button class="ui button editStoryBtn" data-tooltip="编辑小说" data-position="bottom center"><i class="edit icon"></i></button>' +
                    '<button class="ui button deleteStoryBtn" data-tooltip="删除小说" data-position="bottom center"><i class="trash alternate icon"></i></button>' +
                    downloadBtn +
                    '</div>';
                return html;
            }
        }],
    });

    $('#storyTable').on("click", ".updateStoryBtn", function() {
        var id = $(this).parent(".buttons").data("storyid");
        var lastChapter = $(this).parents("td").prev("td");
        var title = $(this).parents("tr").find("td:first").text();
        alert("正在更新小说" + title + "，请稍候...");
        $.get("{url('/update/')}" + id, function() {
            alert("更新" + title + "完毕");
            storyTable.ajax.reload();
        });
    });

    $('#storyTable').on("click", ".deleteStoryBtn", function() {
        var id = $(this).parent(".buttons").data("storyid");
        var lastChapter = $(this).parents("td").prev("td");
        var title = $(this).parents("tr").find("td:first").text();
        console.log(title)
        confirms({
            title: "警告",
            content: "确认删除" + title + "?",
            'okBtn': {
                class: "green",
                name: "确认",
                action: function() {
                    $.get("{url('/admin/story/delete/')}" + id, function(data) {
                        data = $.parseJSON(data);
                        alert(data.message);
                        if (data.status != "Error")
                            storyTable.ajax.reload();
                    });
                }
            }
        });
    });

    $("#updateAll").click(function() {
        alert("正在更新所有小说，请稍候...");
        $.get("{url('/update/')}", function(data) {
            alert("更新完毕");
            //storyTable.ajax.reload();
        });
    });
});
</script>