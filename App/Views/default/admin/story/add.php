<div class="ui grid">
    <div class="two column row">
        <div class="column">
            <div class="ui breadcrumb">
                <a class="section" href="{url('/admin')}">管理首页</a>
                <i class="right angle icon divider"></i>
                <a class="section" href="{url('/admin/story')}">小说管理</a>
                <i class="right angle icon divider"></i>
                <a class="section active">{$viewTitle}</a>
            </div>
        </div>
        <div class="column right aligned">
            <div class="ui mini basic icon buttons">
                <a class="ui button" title="新建小说" data-tooltip="新建小说" data-position="bottom center"
                    href="{url('/admin/story/add')}">
                    <i class="icons">
                        <i class="book icon"></i>
                        <i class="top right corner add icon"></i>
                    </i>
                </a>
                <a class="ui button" data-tooltip="上传小说" data-position="bottom center"
                    data-dialog="{url('/admin/story/upload')}">
                    <i class="upload icon"></i>
                </a>
                <a class="ui button" data-tooltip="更新所有网络小说" data-position="bottom right" id="updateAll">
                    <i class="download icon"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="ui divider"></div>