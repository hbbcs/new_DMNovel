<div class="ui three column stackable center aligned grid">
    <div class="column">
        <div class="ui red segment">

            <h2 class="ui icon header">
                <i class="users icon"></i>
                <div class="ui labeled button" tabindex="0">
                    <div class="ui blue basic button">
                        用户
                    </div>
                    <a class="ui basic blue left pointing label">
                        {$users}
                    </a>
                </div>
            </h2>
            <b class="ui header">
            </b>
        </div>
    </div>
    <div class="column">
        <div class="ui yellow segment">
            <h2 class="ui icon header">
                <i class="book icon"></i>
                <div class="ui labeled button" tabindex="0">
                    <div class="ui blue basic button">
                        收录
                    </div>
                    <a class="ui basic blue left pointing label">
                        {$story}
                    </a>
                </div>

            </h2>
        </div>
    </div>
    <div class="column">
        <div class="ui green segment">
            <h2 class="ui icon header">
                <i class="database icon"></i>
                <div class="ui labeled button" tabindex="0">
                    <div class="ui blue basic button">
                        数据库
                    </div>
                    <a class="ui basic blue left pointing label">
                        {$volume}
                    </a>
                </div>
            </h2>
        </div>
    </div>
</div>

<div class="ui yellow stacked segment" style="margin-top:30px">
    <div>
        <canvas id="areaChart" height="100"></canvas>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $.include.set("{$assets}/").file(['Chart.min.js']);
    var lineChartData = {
        type: 'line',
        data: {
            labels: [<?=$chartLabel?>],
            datasets: [{
                    label: "新增小说",
                    fill: false,
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: [<?=$chartStory?>]
                },
                {
                    label: "主页流量",
                    fill: false,
                    backgroundColor: 'rgb(54, 162, 235)',
                    borderColor: 'rgb(54, 162, 235)',
                    data: [<?=$chartVisit?>]
                },
                {
                    label: "小说下载",
                    fill: false,
                    backgroundColor: 'rgb(54, 82, 235)',
                    borderColor: 'rgb(54, 82, 235)',
                    data: [<?=$download?>]
                }
            ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: '最近7天站点统计信息'
            },
        }
    }

    new Chart($('#areaChart'), lineChartData);
});
</script>