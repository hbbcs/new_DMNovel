<nav aria-label="breadcrumb">
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item">
            <a class="section" href="{url('/')}" data-pjax="">首页</a>
        </li>
        <li class="breadcrumb-item">
            <a class="section" href="{url('/category/')}{$category['id']}" data-pjax="">{$story['category']}</a>
        </li>
        <li class="breadcrumb-item">
            <a class="section" href="{url('/category/')}{$subcategory['id']}" data-pjax="">{$story['sub_category']}</a>
        </li>
        <li class="breadcrumb-item">
            {$viewTitle}
        </li>
    </ol>
</nav>


<div class="card">
    <div class="card-body">
        <div class="float-left m-2 mr-4">
            <img src="{$assets}/images/covers/{$story['cover']}" class="img-thumbnail" style="height:210px;">
        </div>
        <div class="media-body">
            <div class="header">
                <h2 class="text-muted ">{$story['title']}</h2>

                <a class="badge badge-outline-primary  mr-2 " href="{url('/search')}?key={$story['author']}"
                    data-pjax=true>
                    <i class="mdi mdi-account-tie-outline"></i>
                    {$story['author']}
                </a>

                <a class="badge badge-outline-success" href="javascript:void(0);" id="shelf">
                    <i class="mdi mdi-cards-heart"></i> 收藏
                    {$story['favorites']}
                </a>
                <a class="badge badge-outline-success" href="javascript:void(0);" id="score">
                    <i class="mdi mdi-star"></i> 投票
                    {$story['vote']}
                </a>


                <a class="badge badge-outline-success">
                    <i class="mdi mdi-eye"></i> 流量
                    {$story['click']}
                </a>
                <span class="dropdown" href="javascript:void(0);">
                    <a class="badge badge-outline-success dropdown-toggle" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="mdi mdi-download"></i> 下载
                    </a>
                    <div class="dropdown-menu dropdown-menu-sm-left" id="download" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#" data-type="txt">Txt 格式</a>
                        <a class="dropdown-item" href="#" data-type="epub">Epub格式</a>
                    </div>
                </span>
            </div>

            <div class="description">
                <p>{$story['desc']}</p>
            </div>
            <div class="extra">
                {if $story['last_chapter']}
                <p>
                    最后章节：{$story['last_chapter']}
                    (<?=date("m月d日 H:i 更新", $story['last_update'])?>)
                </p>
                {/if}

            </div>
        </div>
    </div>
</div>

<div class="card" data-tab="chapters" style="min-height:300px;">
    <div class="card-body">
        <div class="dropdown float-right">
            <a href="widgets.html#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown"
                aria-expanded="false">
                <i class="mdi mdi-dots-vertical"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" id="showChapter">
                <!-- item-->
                <a href="javascript:void(0);" class="dropdown-item" data-type="0">所有章节</a>
                <!-- item-->
                <a href="javascript:void(0);" class="dropdown-item" data-type="1">最新章节</a>
            </div>
        </div>
        <h4 class="header-title mb-2">最新章节</h4>
        <div class="ui text loadChapter">正在从网络下载章节<i></i>，请稍候...</div>
    </div>
</div>


<div class="card" style="" data-tab="comment" id="commentContent">
    <div class="card-body">
        {foreach $comments as $comment}
        <div class="media mt-2 pt-2 pl-2 pr-0" data-comment-id="{$comment['id']}">
            <img src="{$assets}/images/avatar/{$comment['avatar']?:'default.jpg'}"
                class="rounded-circle avatar-sm img-thumbnail float-left mr-2">
            <div class="media-body">
                <h5 class="mt-0 mb-0 font-14">
                    <span class="float-right text-muted font-12">
                        <?=date("Y-m-d H:m", $comment['date'])?>
                    </span>
                    {$comment['username']}
                </h5>

                <p class="mt-1 mb-0 text-muted font-16" style="min-height:40px;">
                    {if $comment['commend']}
                    <span class="w-25 float-right text-right">
                        <span class="badge badge-danger-lighten" title="回复数">
                            {$comment['commend']}
                        </span>
                    </span>
                    {/if}
                    <?=preg_replace("/:([a-z\-\_]+):/i", "<i class='em em-$1'></i>", $comment['content']);?>
                </p>
                <div class="font-12">
                    <a class="reply-content text-muted mr-3" href="javascript:void(0);">
                        <i class="mdi mdi-message-reply-text"></i>
                        {$comment['replyNum']} 回复
                    </a>

                    <a class="text-muted mr-3" href="javascript:void(0);">
                        <i class="mdi  mdi-thumb-up-outline"></i>
                        {$comment['commend']} 赞
                    </a>

                    <a class="reply text-muted" href="javascript:void(0);">
                        <i class="mdi mdi-reply"></i>
                        发表回复
                    </a>
                </div>
                <blockquote class="d-none"></blockquote>
            </div>
        </div>
        {/foreach}

        <div class="border rounded mt-2 mb-3" id="commentFormDiv">
            <form action="{url('/comment/post')}" method="post" id="commentForm" class="comment-area-box">
                <textarea rows="5" name="content" class="form-control border-0 resize-none"
                    placeholder="评论内容，最多500字符...."></textarea>
                <input type="hidden" name="id" value="{$story['id']}" />
                <input type="hidden" name="reply" />
                <div class="p-2 bg-light d-flex justify-content-between align-items-center">
                    <div>
                        <a href="javascript:void(0);" class="btn btn-sm px-2 font-16 btn-light" id="expression">
                            <i class="mdi mdi-emoticon-outline"></i>
                        </a>
                    </div>
                    <div class="right">
                        <button type="button" class="btn btn-sm waves-effect" id="closeComment" hidden>关闭</button>
                        <span id="textCount" class="font-12 mr-2"><i>0</i>/500</span>
                        <button type="submit" class="btn btn-sm btn-dark waves-effect">发表</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Vote Popup -->
<div class="popover vote" hidden>
    <div class="header">投票</div>
    <div class="content">一次最多投5票，票数是累计的</div>
    <div class="ui star rating" data-rating="3"></div>
</div>
<!-- end vote -->
<!-- Popup -->
<div class="popover expression" hidden id="expressionContent">
    <div class="content expression">
        <i class="em em-smile" data-em="smile"></i>
        <i class="em em-laughing" data-em="laughing"></i>
        <i class="em em-blush" data-em="blush"></i>
        <i class="em em-smiley" data-em="smiley"></i>
        <i class="em em-relaxed" data-em="relaxed"></i>
        <i class="em em-smirk" data-em="smirk"></i>
        <i class="em em-heart_eyes" data-em="heart_eyes"></i>
        <i class="em em-kissing_heart" data-em="kissing_heart"></i>
        <i class="em em-kissing_closed_eyes" data-em="kissing_closed_eyes"></i>
        <i class="em em-flushed" data-em="flushed"></i>
        <i class="em em-relieved" data-em="relieved"></i>
        <i class="em em-satisfied" data-em="satisfied"></i>
        <i class="em em-grin" data-em="grin"></i>
        <i class="em em-wink" data-em="wink"></i>
        <i class="em em-stuck_out_tongue_winking_eye" data-em="stuck_out_tongue_winking_eye"></i>
        <i class="em em-stuck_out_tongue_closed_eyes" data-em="stuck_out_tongue_closed_eyes"></i>
        <i class="em em-grinning" data-em="grinning"></i>
        <i class="em em-kissing" data-em="kissing"></i>
        <i class="em em-kissing_smiling_eyes" data-em="kissing_smiling_eyes"></i>
        <i class="em em-stuck_out_tongue" data-em="stuck_out_tongue"></i>
        <i class="em em-sleeping" data-em="sleeping"></i>
        <i class="em em-worried" data-em="worried"></i>
        <i class="em em-frowning" data-em="frowning"></i>
        <i class="em em-anguished" data-em="anguished"></i>
        <i class="em em-open_mouth" data-em="open_mouth"></i>
        <i class="em em-grimacing" data-em="grimacing"></i>
        <i class="em em-confused" data-em="confused"></i>
        <i class="em em-hushed" data-em="hushed"></i>
        <i class="em em-expressionless" data-em="expressionless"></i>
        <i class="em em-unamused" data-em="unamused"></i>
        <i class="em em-sweat_smile" data-em="sweat_smile"></i>
        <i class="em em-sweat" data-em="sweat"></i>
        <i class="em em-disappointed_relieved" data-em="disappointed_relieved"></i>
        <i class="em em-weary" data-em="weary"></i>
        <i class="em em-pensive" data-em="pensive"></i>
        <i class="em em-disappointed" data-em="disappointed"></i>
        <i class="em em-confounded" data-em="confounded"></i>
        <i class="em em-fearful" data-em="fearful"></i>
        <i class="em em-cold_sweat" data-em="cold_sweat"></i>
        <i class="em em-persevere" data-em="persevere"></i>
        <i class="em em-cry" data-em="cry"></i>
        <i class="em em-sob" data-em="sob"></i>
        <i class="em em-joy" data-em="joy"></i>
        <i class="em em-astonished" data-em="astonished"></i>
        <i class="em em-scream" data-em="scream"></i>
        <i class="em em-tired_face" data-em="tired_face"></i>
        <i class="em em-angry" data-em="angry"></i>
        <i class="em em-rage" data-em="rage"></i>
        <i class="em em-triumph" data-em="triumph"></i>
        <i class="em em-sleepy" data-em="sleepy"></i>
        <i class="em em-yum" data-em="yum"></i>
        <i class="em em-mask" data-em="mask"></i>
        <i class="em em-sunglasses" data-em="sunglasses"></i>
        <i class="em em-dizzy_face" data-em="dizzy_face"></i>
        <i class="em em-imp" data-em="imp"></i>
        <i class="em em-smiling_imp" data-em="smiling_imp"></i>
        <i class="em em-neutral_face" data-em="neutral_face"></i>
        <i class="em em-no_mouth" data-em="no_mouth"></i>
        <i class="em em-innocent" data-em="innocent"></i>
        <i class="em em-alien" data-em="alien"></i>
        <i class="em em-yellow_heart" data-em="yellow_heart"></i>
        <i class="em em-blue_heart" data-em="blue_heart"></i>
        <i class="em em-purple_heart" data-em="purple_heart"></i>
        <i class="em em-heart" data-em="heart"></i>
        <i class="em em-green_heart" data-em="green_heart"></i>
        <i class="em em-broken_heart" data-em="broken_heart"></i>
        <i class="em em-heartbeat" data-em="heartbeat"></i>
        <i class="em em-heartpulse" data-em="heartpulse"></i>
        <i class="em em-two_hearts" data-em="two_hearts"></i>
        <i class="em em-revolving_hearts" data-em="revolving_hearts"></i>
        <i class="em em-cupid" data-em="cupid"></i>
        <i class="em em-sparkling_heart" data-em="sparkling_heart"></i>
        <i class="em em-sparkles" data-em="sparkles"></i>
        <i class="em em-star" data-em="star"></i>
        <i class="em em-star2" data-em="star2"></i>
        <i class="em em-dizzy" data-em="dizzy"></i>
        <i class="em em-boom" data-em="boom"></i>
        <i class="em em-collision" data-em="collision"></i>
        <i class="em em-anger" data-em="anger"></i>
        <i class="em em-exclamation" data-em="exclamation"></i>
        <i class="em em-question" data-em="question"></i>
        <i class="em em-grey_exclamation" data-em="grey_exclamation"></i>
        <i class="em em-grey_question" data-em="grey_question"></i>
        <i class="em em-zzz" data-em="zzz"></i>
        <i class="em em-dash" data-em="dash"></i>
        <i class="em em-sweat_drops" data-em="sweat_drops"></i>
        <i class="em em-notes" data-em="notes"></i>
        <i class="em em-musical_note" data-em="musical_note"></i>
        <i class="em em-fire" data-em="fire"></i>
        <i class="em em-hankey" data-em="hankey"></i> </div>
</div>

<script type="text/javascript">
$(function() {
    $.include.set("{$assets}/").file(["apple.css", "jquery.validator.js"]);
    $("div.loadChapter").load("{url('/book/chapters/')}{$story['id']}/0/1");

    $("#score").popover({
        content: $('.popover.vote').html(),
        title: $('.popover.vote .header').text(),
        html: true,
        placement: "bottom"
    });

    $("#expression").popover({
        content: $("#expressionContent").html(),
        html: true,
        trigger: 'click',
        placement: "auto"
    })

    $("body").on('click', '.content.expression i', function() {
        var str = $(this).attr("class");
        var emoji = str.substring(6, str.length);

        $("textarea[name=content]").val($("textarea[name=content]").val() + ":" + emoji + ":")
            .focus();
        $("#expression").popover("hide");
    });

    $(".submit.button").click(function() {
        $(this).parents("form").submit();
    });

    //发表评论和回复
    $('#commentForm').validator({
        success: function(data) {

            $.toast({
                heading: data.status,
                position: 'top-center',
                text: data.message,
                icon: data.status == "Error" ? "warning" : "success"
            });
            if (data.status != "Error") {
                if (data.reply != 0) {
                    $("#commentForm").parents("div.media-body:first").children("blockquote").append(
                        data
                        .html);
                    $("#closeComment").click();
                    $("textarea[name=content]").val("");
                } else {
                    $("#commentContent .card-body").prepend(data.html);
                }
            }
        }
    });

    $("#download .dropdown-item").click(function() {
        var type = $(this).data("type");
        $.post("{url('/download/')}", {
            'id': "{$story['id']}",
            'type': type
        }, function(data) {
            var e = $.parseJSON(data);
            if (e.message)
                alert(e.message, e.status);
            if (e.status != "error") {
                alert("文件已生成，请下载...");
                window.location.href = "{url('/download/get/')}" + e.path + "/" + e.name;
            }
        });
    });

    $("body").on('input propertychange', 'textarea[name=content]', function() {
        var content = $(this).val();
        $("#textCount").children("i").html(content.length)
        if (content.length > 500) {
            alert("内容长度最多500字符...");
            $(this).val(content.substr(0, 100));
        }
    });

    $("#showChapter a").click(function() {
        var type = $(this).data("type");
        $("div.loadChapter").load("{url('/book/chapters/')}{$story['id']}/0/" + type);
        $(this).parents(".card-body").find("h4.header-title").html($(this).html());

    })

    $("#shelf").click(function() {
        var favo = '{$favorites ? "true" : "false"}';
        var num = "{$story['favorites']}";
        var $this = $(this);
        $.get("{url('/shelf/favorites/')}{$story['id']}", function(data) {
            data = $.parseJSON(data);
            alert(data.message, data.status);
            if (data.status != "error") {
                if (favo) {
                    $this.children(".button").removeClass("yellow");
                    $this.children("aa").html(num - 1);
                } else {
                    $this.children(".button").addClass("yellow");
                    $this.children("a").html(num + 1);
                }
            }
        })
    });
    //评论发表回复
    $("body").on("click", "a.reply", function() {
        $("textarea[name=content]").focus();
        $("input[name=reply]").val($(this).parents("div.media:first").data("comment-id"));
        $("#commentFormDiv").appendTo($(this).parents("div.media-body:first"));
        $("#closeComment").removeAttr("hidden");
    });

    $("body").on("click", "#closeComment", function() {
        $("#closeComment").attr("hidden", true);
        $("#commentFormDiv").appendTo($("#commentContent .card-body:first"));
    })
    //查看评论回复
    $("body").on("click", "a.reply-content", function() {
        var id = $(this).parents("div.media:first").data("comment-id");
        var blockquote = $(this).parents("div.media-body:first").children("blockquote");
        if (blockquote.html() != "") {
            blockquote.slideToggle();
        } else {
            blockquote.load("{url('/comment/get/')}" + id).removeClass("d-none").slideDown();
        }
    })

    $('.ui.popup .rating').rating({
        initialRating: 1,
        maxRating: 5,
        onRate: function(value) {
            $.post("{url('/vote')}", {
                story: "{$story['id']}",
                value: value
            }, function(data) {
                data = $.parseJSON(data);
                $.toast({
                    heading: data.status,
                    position: 'top-center',
                    text: data.message,
                    icon: data.status == "Error" ? "warning" : "success"
                });
            })
        }
    });


});
</script>