<table class="ui chapterList single line table ">
    <tbody>
        <tr>
            <?php $i = 0;
            $count = count($chapters) ?>
            {foreach $chapters as $chapter}
            <td title="{$chapter['title']}">
                <a href="{url('/read/')}{$storyid}/{$chapter['id']}">{$chapter['title']}</a>
            </td>
            <?php $i++; ?>
            {if $i%3==0 && $i<$count} </tr> <tr>
                {/if}
                {/foreach}
        </tr>
    </tbody>
</table>