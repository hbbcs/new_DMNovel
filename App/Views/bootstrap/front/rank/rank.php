<div class="ui breadcrumb">
    <a class="section" href="{url('/')}" data-pjax="">首页</a>
    <i class="right chevron icon divider"></i>
    <div class="active section">排行榜</div>
</div>

<div class="ui pointing secondary menu" id="rankMenu" style="position:sticky;top:56px;background:#fafafa;z-index:99;">
    <a class="item active" data-id="">
        <i class="icon server"></i>
        本地得票榜
    </a>
    {foreach $ranks as $rank}
    <a class="item" data-id="{$rank['id']}">{$rank['title']}</a>
    {/foreach}
</div>

<div class="ui segment" style="min-height:300px;">
    <div class="ui relaxed divided list" id="rankList">
        <div class="ui active inverted dimmer">
            <div class="ui text loader">Loading</div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $("#rankList").load("{url('/rank/get')}");
    $(window).scroll(function() {
        $(".miniCover[data-isLoaded!=1]").imageLoad();
    });

    $('#rankMenu a').click(function() {
        var id = $(this).data('id');
        $('#rankMenu').find(".active").removeClass("active");
        $(this).addClass("active");
        $("#rankList").load("{url('/rank/get/')}" + id);
    });

});
</script>