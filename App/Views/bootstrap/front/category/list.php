{foreach $stories['books'] as $story}
<div class="list-group-item">
    <img src="{$assets}/images/covers/default.jpg"
        data-src="{($type!='cloud') ?$assets.'/images/covers/':$staticUrl}{$story['cover']}" class="miniCover" />
    <div class="content">
        <a class="header <?=$type == 'local' ?: 'cloud'?>"
            href="{url('/book/')}<?=isset($story['id']) ? $story['id'] : $story['_id']?><?=$type == 'local' || !$type ? "" : '/cloud'?>"
            data-pjax=true>
            <h3>{$story['title']}</h3>
        </a>
        <div class="description">

            <a class="badge badge-outline-success" href="{url('/search')}?key={$story['author']}" data-pjax=true
                title="作者">
                <i class="mdi mdi-account-tie-outline"></i>
                {$story['author']}
            </a>

            {if $type=="cloud"}
            <div class="badge badge-outline-success" tabindex="0" title="留存率">

                <i class="icon heart"></i>
                {$story['retentionRatio']}

            </div>
            <div class="ui labeled mini button" tabindex="0" title="追书者">
                <div class="ui mini icon button">
                    <i class="icon flag"></i>
                </div>
                <a class="ui basic left pointing label">
                    {$story['latelyFollower']}
                </a>
            </div>
            {/if}
        </div>
        <div class="description">
            简介：<?=isset($story['shortIntro']) ? $story['shortIntro'] : $story['desc']?>
        </div>
    </div>
</div>
{/foreach}
{if $page==0}
<script type="text/javascript">
$(function() {
    $(".miniCover[data-isLoaded!=1]").imageLoad();
    $("#categoryBooksList").data("total", < ? = $stories['total'] ? > ).data("type", '<?=$type ?: "local"?>')
});
</script>
{/if}