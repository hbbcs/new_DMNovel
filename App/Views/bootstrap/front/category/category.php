<nav aria-label="breadcrumb">
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item">
            <a class="section" href="{url('/')}" data-pjax="">
                <i class="mdi mdi-home"></i>
                首页
            </a>
        </li>
        {foreach $categoryTitle as $t}
        <li class="breadcrumb-item">
            <a class="section" href="{url('/category/')}{$t['id']}" data-pjax="">
                <i class="icon {$t['icon']}"></i>
                {$t['name']}
            </a>
        </li>
        {/foreach}
    </ol>
</nav>


<ul class="nav nav-tabs nav-bordered mb-3" id="categoryMenu">
    <li class="nav-item">
        <a class="nav-link active" data-type="local">
            <i class="icon server"></i>
            本地
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-type="cloud">
            <i class="icon cloud"></i>
            追书
        </a>
    </li>
</ul>

<div class="row">
    <div class="<?=count($sub) > 0 ? "col-lg-10" : "col-lg-12"?>">
        <div class="card">
            <div class="list-group" id="categoryBooksList" data-total="{$stories['total']}" data-page="{$page}"
                data-type="{$type}">
                <?=$list ?: "<h1>当前分类没有小说</h1>"?>
            </div>
        </div>
    </div>
    {if count($sub)>0}
    <div class="col-lg-2">
        <div class="list-group" id="subCateMenu">
            <div class="list-group-item active">
                <i class="icon th list"></i>
                子分类
            </div>

            {foreach $sub as $s}
            <div class="list-group-item">
                <h5>
                    <a href="{url('/category/'.$s['id'])}" data-pjax="true">{$s['name']}</a>
                </h5>
            </div>
            {/foreach}
        </div>
    </div>
    {/if}
</div>

<script type="text/javascript">
$(function() {
    //$("#categoryList").load("{url('/category/get/')}{$category}");
    $(window).scroll(function() {
        $(".miniCover[data-isLoaded!=1]").imageLoad();
    });

    $('#categoryMenu a').click(function() {
        $('#categoryMenu').find(".active").removeClass("active");
        $(this).addClass("active");
        var type = $(this).data("type");

        $("#categoryBooksList").data("type", type).addClass("loading");
        $.post("{url('/category/get/')}{$category}", {
            page: 0,
            'type': type
        }, function(data) {
            $("#categoryBooksList").html(data ? data : "<h1>当前分类没有小说</h1>")
                .removeClass("loading");
        });
    })

    $(document).scroll(function() {
        $(".miniCover[data-isLoaded!=1]").imageLoad();
        var htmlHeight = document.body.scrollHeight || document.documentElement.scrollHeight;
        //clientHeight是网页在浏览器中的可视高度，
        var clientHeight = document.body.clientHeight || document.documentElement.clientHeight;
        //scrollTop是浏览器滚动条的top位置，
        var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
        var allPage = Math.ceil(parseInt($('#categoryBooksList').data("total")) / 20)
        var page = parseInt($('#categoryBooksList').data("page")) + 1;
        var type = $('#categoryBooksList').data("type");

        var message;
        //通过判断滚动条的top位置与可视网页之和与整个网页的高度是否相等来决定是否加载内容；
        if (scrollTop + clientHeight == htmlHeight) {
            if (allPage > page) {
                message = "正在加载，请稍候..."
                $.post("{url('/category/get/')}{$category}?", {
                    'type': type ? type : "local",
                    'page': page,
                }, function(data) {
                    $('#categoryBooksList').data("page", page).append(data);
                });
            } else {
                message = "已全部加载完毕";
            }
            alert(message, "success");
        }
    });
});
</script>