<div class="ui floating message w50 hcenter">
    <form class="ui form" action="{url('/user/password')}" method="post" id="passwordForm">
        <div class="field">
            <label>原密码</label>
            <input type="password" name="oldpassword" placeholder="原密码" data-required=true>
        </div>

        <div class="field">
            <label>新密码</label>
            <div class="two fields">
                <div class="field">
                    <input type="password" name="newpassword" placeholder="输入密码，不小于3位" data-required=true>
                </div>
                <div class="field">
                    <input type="password" name="repassword" placeholder="重复密码" data-required=true
                        data-equal="input[name=newpassword]">
                </div>
            </div>
        </div>
        <button class="ui button" type="submit">修改密码</button>
    </form>
</div>

<script type="text/javascript">
$(function() {
    $.include.set("{$assets}/").file("jquery.validator.js");

    $('#passwordForm').validator({
        success: function(data) {
            $.toast({
                heading: data.status,
                position: 'top-right',
                text: data.message,
                icon: data.status == "Error" ? "warning" : "success"
            })
        }
    });
});
</script>
