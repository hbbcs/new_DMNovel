<form class="ui large form error" action="{url('/user/register')}" method="post" id="registerForm">
    <div class="field">
        <label>用户名</label>
        <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="username" placeholder="用户名" data-required='true'>
        </div>
    </div>
    <div class="field">
        <label>密码</label>
        <div class="two fields">
            <div class="ui left icon input field">
                <i class="lock icon"></i>
                <input type="password" name="password" placeholder="密码" data-required="true">
            </div>
            <div class="field">
                <input type="password" name="repassword" placeholder="重复密码" data-required="true">
            </div>
        </div>
    </div>
    <div class="field">
        <label>邮箱</label>
        <div class="ui left icon input">
            <i class="mail icon"></i>
            <input type="text" name="email" placeholder="邮箱地址" data-required="true">
        </div>
    </div>
    <button class="ui primary fluid large teal submit button" type="submit">注册</button>
</form>
<script>
$(function() {
    $.include.set("{$assets}/").file("jquery.validator.js");
    $('#registerForm').validator({
        success: function(data) {
            $.toast({
                heading: data.status,
                position: 'top-center',
                text: data.message,
                icon: data.status == "Error" ? "warning" : "success"
            });
            if (data.status != "Error") {
                $.post("{url('/user/login')}", {
                    'username': $("input[name=username]").val(),
                    'password': $("input[name=password]").val()
                }, function() {
                    window.location.href = '{url("/")}';
                })
            }
        }
    });

});
</script>