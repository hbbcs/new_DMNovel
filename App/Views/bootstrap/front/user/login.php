<div class="row justify-content-center">
    <div class="col-lg-5">
        <div class="card">

            <!-- Logo -->
            <div class="card-header pt-4 pb-4 text-center bg-primary">
                <a href="index.html">
                    <h4 class="text-white">{$title}</h4>
                </a>
            </div>

            <div class="card-body p-4">

                <div class="text-center w-75 m-auto">
                    <h4 class="text-dark-50 text-center mt-0 font-weight-bold">登入网站</h4>
                    <p class="text-muted mb-4">输入用户名和密码访问网站内容。</p>
                </div>

                <form action="{url('/user/login')}" method="post" id="loginForm">
                    <div class="form-group">
                        <label for="emailaddress">用户名</label>
                        <input class="form-control" type="text" name="username" placeholder="用户名" data-required='true'>
                    </div>
                    <div class="form-group">
                        <a href="{url('user/lost')}" class="text-muted float-right"><small>忘记密码?</small></a>
                        <label for="password">密码</label>
                        <input class="form-control" type="password" name="password" placeholder="密码"
                            data-required='true'>
                    </div>
                    <div class="form-group mb-3">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="checkbox-signin">
                            <label class="custom-control-label" name="remember" for="checkbox-signin">Remember
                                me</label>
                        </div>
                    </div>
                    <div class="form-group mb-0 text-center">
                        <button class="btn btn-primary btn-block" type="submit"><i class="mdi mdi-login"></i> Log In
                        </button>
                    </div>
                </form>
            </div> <!-- end card-body -->
        </div>
        <!-- end card -->

        <div class="row mt-3">
            <div class="col-12 text-center">
                <p class="text-muted">您还没有账号? <a href="pages-register.html" class="text-muted ml-1"><b>现在注册</b></a></p>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    </div> <!-- end col -->
</div>
<!-- end auth-fluid-->

<script type="text/javascript">
$(function() {
    $.include.set("{$assets}/").file("jquery.validator.js");

    $('#loginForm').validator({
        success: function(data) {
            $.toast({
                heading: data.status,
                position: 'top-center',
                text: data.message,
                icon: data.status == "Error" ? "warning" : "success"
            });
            if (data.status != "Error") {
                window.location.href = '{url("/")}';
            }
        }
    });
});
</script>