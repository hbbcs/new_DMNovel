<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title><?=isset($viewTitle) ? $viewTitle . " - " : "";?>{$title}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{$assets}/images/favicon.ico">

    <!-- third party css -->
    <link href="{$assets}/css/bootstrap/vendor/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
    <script src="{$assets}/js/jquery.min.js"></script>

    <!-- App css -->
    <link href="{$assets}/css/bootstrap/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="{$assets}/css/bootstrap/app-modern.min.css" rel="stylesheet" type="text/css" id="style" />
    <link href="{$assets}/css/bootstrap/main.css" rel="stylesheet" type="text/css" />
</head>

<body data-layout="topnav">

    <!-- Begin page -->
    <!-- Topbar Start -->
    <div class="navbar-custom topnav-navbar">
        <div class="container-lg">
            <!-- LOGO -->
            <a href="layouts-horizontal.html" class="topnav-logo">
                <span class="topnav-logo-lg">
                    <img src="{$assets}/images/logo.png" alt="" height="36">
                </span>
                <span class="topnav-logo-sm">
                    <img src="{$assets}/images/logo-mini.png" alt="" height="16">
                </span>
            </a>

            <ul class="list-unstyled topbar-right-menu float-right mb-0">

                <li class="notification-list">
                    <a class="nav-link dark-ligth" href="javascript: void(0);">
                        <i class="mdi mdi-theme-light-dark noti-icon"></i>
                    </a>
                </li>

                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#"
                        id="topbar-notifydrop" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="mdi mdi-bell noti-icon"></i>
                        <span class="noti-icon-badge"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-lg"
                        aria-labelledby="topbar-notifydrop">

                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5 class="m-0">
                                <span class="float-right">
                                    <a href="javascript: void(0);" class="text-dark">
                                        <small>Clear All</small>
                                    </a>
                                </span>Notification
                            </h5>
                        </div>

                        <div style="max-height: 230px;" data-simplebar>
                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <div class="notify-icon bg-primary">
                                    <i class="mdi mdi-comment-account-outline"></i>
                                </div>
                                <p class="notify-details">Caleb Flakelar commented on Admin
                                    <small class="text-muted">1 min ago</small>
                                </p>
                            </a>
                        </div>

                        <!-- All-->
                        <a href="javascript:void(0);"
                            class="dropdown-item text-center text-primary notify-item notify-all">
                            View All
                        </a>

                    </div>
                </li>
                {if isset($user)}
                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle nav-user arrow-none mr-0" data-toggle="dropdown"
                        id="topbar-userdrop" href="layouts-horizontal.html#" role="button" aria-haspopup="true"
                        aria-expanded="false">
                        <span class="account-user-avatar">
                            <img src="{$assets}/images/avatar/{$user['avatar']?:'default.jpg'}" alt="user-image"
                                class="rounded-circle">
                        </span>
                        <span>
                            <span class="account-user-name">{$user['username']}</span>
                            <span class="account-position">{$user['group']['name']}</span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated topbar-dropdown-menu profile-dropdown"
                        aria-labelledby="topbar-userdrop">
                        <!-- item-->
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">欢迎您 !</h6>
                        </div>

                        <!-- item-->
                        <a href="{url('/user/profile')}" class="dropdown-item notify-item">
                            <i class="mdi mdi-account-circle mr-1"></i>
                            <span>我的账号</span>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <i class="mdi mdi-account-edit mr-1"></i>
                            <span>站点设置</span>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <i class="mdi mdi-lifebuoy mr-1"></i>
                            <span>Support</span>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <i class="mdi mdi-lock-outline mr-1"></i>
                            <span>Lock Screen</span>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <i class="mdi mdi-logout mr-1"></i>
                            <span>退出登录</span>
                        </a>

                    </div>
                </li>
                {else}
                <li class="notification-list">
                    <a class="nav-link" href="{url('/user')}">
                        <i class="mdi mdi-login noti-icon"></i>
                        登录
                    </a>
                </li>
                {/if}
            </ul>
            <a class="navbar-toggle" data-toggle="collapse" data-target="#topnav-menu-content">
                <div class="lines">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </a>

            <div class="app-search">
                <form action="{url('/search')}" method="post" id="searchForm">
                    <div class="input-group">
                        <input type="text" class="form-control search" name="key" placeholder="搜索作品或作者">
                        <span class="mdi mdi-magnify"></span>
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">
                                <i class="mdi mdi-book-search mdi-18px"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- end Topbar -->

    <div class="topnav sticky-top">
        <div class="container-fluid active">
            <nav class="navbar navbar-dark navbar-expand-lg topnav-menu">
                <div class="collapse navbar-collapse" id="topnav-menu-content">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link " href="{url('/')}">
                                <i class="mdi mdi-home mr-1"></i>
                                首页
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle arrow-none" href="layouts-horizontal.html#"
                                id="topnav-apps" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="mdi mdi-library mr-1"></i>
                                分类
                                <div class="arrow-down"></div>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="topnav-apps">
                                {if isset($categories)}
                                {foreach $categories as $category}
                                <a href="{url('/category/'.$category['id'])}" class="dropdown-item" data-pjax="">
                                    <i class="icon {$category['icon']}"></i>
                                    {$category['name']}
                                </a>
                                {/foreach}
                                {/if}
                        <li class="nav-item dropdown">
                            <a class="nav-link " href="{url('/rank')}">
                                <i class=" mdi mdi-medal mr-1"></i>
                                排行
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link " href="{url('/rank')}">
                                <i class=" mdi mdi-book mr-1"></i>
                                完本
                            </a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link " href="{url('/shelf')}">
                                <i class="mdi mdi-bookshelf mr-1"></i>
                                书架
                            </a>
                        </li>
                    </ul>
                </div>

            </nav>
        </div>
    </div>

    <div class="wrapper">

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">
                <!-- Start Content-->
                <div class="container" id="mainContent">
                    {$content}
                </div>
                <!-- content -->

            </div>
            <!-- container -->


            <!-- Footer Start -->
            <footer class="footer">
                <div claui four ss="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            2018 - 2020 © Hyper - Coderthemes.com
                        </div>
                        <div class="col-md-6">
                            <div class="text-md-right footer-links d-none d-md-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->


    <!-- bundle -->
    <script type="text/javascript" src="{$assets}/js/jquery.pjax.js"></script>
    <script type="text/javascript" src="{$assets}/js/popper.min.js"></script>
    <script type="text/javascript" src="{$assets}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{$assets}/js/jquery.toast.min.js"></script>
    <script type="text/javascript" src="{$assets}/js/jquery.Timer.js"></script>
    <script type="text/javascript" src="{$assets}/js/bootstrap.autocomplete.min.js"></script>
    <script type="text/javascript" src="{$assets}/js/nprogress.js"></script>
    <script type="text/javascript" src="{$assets}/js/function.js"></script>
    <script type="text/javascript" src="{$assets}/js/include.js"></script>
    <script type="text/javascript">
    $(function() {

        if (store("DMNovel_Theme") == "dark") {
            $("#style").attr("href", "{$assets}/css/bootstrap/app-modern-dark.min.css");
        } else {
            $("#style").attr("href", "{$assets}/css/bootstrap/app-modern.min.css");
        }
        $(".dark-ligth").click(function(e) {
            e.preventDefault();

            if ($("body").hasClass("dark")) {
                $("#style").attr("href", "{$assets}/css/bootstrap/app-modern.min.css");
                store("DMNovel_Theme", "light");
            } else {
                $("#style").attr("href", "{$assets}/css/bootstrap/app-modern-dark.min.css");
                store("DMNovel_Theme", "dark");
            }
            $("body").toggleClass("dark").toggleClass("light");

        });

        $('#searchForm').submit(function() {
            var key = $("input[name=key]").val();
            if (key == "") {
                alert('请输入搜索的关键字', "warning");
                return false;
            }
        });
        $("#searchForm input[name=key]").autocomplete({
            source: '{url("/search/key")}?q={query}',
            highlightClass: 'text-danger'
        }).change(function() {
            console.log($(this).val())
        });
    });
    </script>

</body>

</html>