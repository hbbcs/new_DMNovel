<div class="row justify-content-md-center">
    <div class="col-lg-4">
        <div class="card widget-flat" style="height:372px">
            <div class="card-body bg-info">

                <div class="float-right">
                    <i class="mdi mdi-bullhorn-outline widget-icon bg-danger rounded-circle text-white"></i>
                </div>
                <h4 class="header mb-0"" title=" 站内公告">站内公告</h4>

                <div id="carouselSlides" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        {if count($notices)>0}
                        {$i = 0;}
                        {foreach $notices as $notice}
                        <div class="carousel-item <?=$i == 0 ? "active" : ""?>" data-interval="30000">
                            <h3 class="mt-0">
                                <a href="javascript:void(0);" data-dialog="{url('/notice/'.$notice['id'])}"
                                    data-content="fjkladh" title="站内公告">
                                    <?=date("m月d日", $notice['date'])?>
                                </a>
                            </h3>
                            <h5 class="font-weight-normal mt-0 notice text-white-50">
                                {$notice['content']}
                            </h5>
                        </div>
                        {$i++}
                        {/foreach}
                        {else}
                        <div class="carousel-item">暂无公告 </div>
                        {/if}
                    </div>

                </div>
                {if count($notices)>0 }
                <ol class="carousel-indicators" id="carouselSlidesIndex">
                    {for $i=0;$i<count($notices);$i++} <li data-target="#carouselSlides" data-slide-to="{$i}"
                        class="<?=$i == 0 ? "active" : ""?>">
                        </li>
                        {/for}
                </ol>
                {/if}
            </div>
            <!-- end card-body -->
        </div>
    </div>
    <div class="col-lg-8">
        <div class="card-deck-wrapper hot">
            <div class="card-deck">
                {$i = 1}
                {foreach $hots as $story}
                <div class="card ribbon-box" data-title="{$story['title']}-{$story['author']}"
                    data-content="{$story['desc']}" data-position="right center">
                    <a class="image" href="{url('/book/')}{$story['id']}" data-pjax="">
                        <img src="{$assets}/images/covers/{$story['cover']}" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <div class="ribbon-two ribbon-two-success"><span>{$i}</span></div>
                        <span class="tag-li float-left" title="收藏">
                            <i class="icon-shelf"></i>
                            {$story['favorites']}
                        </span>
                        <span class="tag-li float-right" title="得票">
                            <i class="star icon"></i>
                            {$story['vote']}
                        </span>
                    </div>
                </div>
                {$i++}
                {/foreach}
            </div>
        </div>
    </div>
</div>


<div class="row justify-content-md-center">

    {foreach $categories as $category}
    <div class="col-lg-4">
        <div class="card shadow">
            <h4 class="card-header">
                <i class="icon {$category['icon']}"></i>
                {$category['name']}
            </h4>
            <div class="card-body category" data-id="{$category['id']}">
                <div>
                    <i class="mdi mdi-spin mdi-loading"></i>Loading...
                </div>
                <p></p>
            </div>
        </div>
    </div>
    {/foreach}

</div>

<script type="text/javascript">
$(function() {

    $("#carouselSlidesIndex").on("click", "li", function() {
        $("#carouselSlidesIndex").find(".active").removeClass("active");
        $(this).addClass("active");
    })

    $('#carouselSlides').on('slide.bs.carousel', function(e) {
        // do something...
        $("#carouselSlidesIndex").find(".active").removeClass("active");
        $("#carouselSlidesIndex").find("li").eq(e.to).addClass("active");
    })

    $(".category").each(function() {
        var $this = $(this);
        var id = $(this).data("id");

        $.get("{url('/hot/')}" + id + "/5", function(data) {
            $listGroup = $("<div>", {
                "class": "list-group list-group-flush"
            })
            if (data) {
                $.each($.parseJSON(data), function(k, v) {
                    var item = $("<div>", {
                            "class": "list-group-item d-flex justify-content-between align-items-center"
                        })
                        .html("<a href='{url('/book/')}" + v.id +
                            "' data-pjax='true'>" + v.title + "</a>")
                        .append(
                            $("<span>", {
                                "class": "badge badge-outline-primary badge-pill",
                                "title": "流量"
                            }).html(v.click)
                        );

                    $listGroup.append(item);
                })
            }
            $this.html("").append($listGroup)
        });
    })
});
</script>