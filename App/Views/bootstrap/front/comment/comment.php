<div class="media pt-2 pl-2 pr-0" data-comment-id="{$comment['id']}">
    <img src="{$assets}/images/avatar/{$comment['avatar']?:'default.jpg'}"
        class="rounded-circle avatar-sm img-thumbnail float-left mr-2">
    <div class="media-body">
        <h5 class="mt-0 mb-0 font-14">
            <span class="float-right text-muted font-12">
                <?=date("Y-m-d H:m", $comment['date'])?>
            </span>
            {$comment['username']}
        </h5>

        <p class="mt-1 mb-0 text-muted font-16" style="min-height:40px;">
            {if $comment['commend']}
            <span class="w-25 float-right text-right">
                <span class="badge badge-danger-lighten" title="回复数">
                    {$comment['commend']}
                </span>
            </span>
            {/if}
            <?=preg_replace("/:([a-z\-\_]+):/i", "<i class='em em-$1'></i>", $comment['content']);?>
        </p>
        <div class="font-12">
            <a class="reply-content text-muted mr-3" href="javascript:void(0);">
                <i class="mdi mdi-message-reply-text"></i>
                {$comment['replyNum']} 回复
            </a>

            <a class="text-muted mr-3" href="javascript:void(0);">
                <i class="mdi  mdi-thumb-up-outline"></i>
                {$comment['commend']} 赞
            </a>

            <a class="reply text-muted" href="javascript:void(0);">
                <i class="mdi mdi-reply"></i>
                发表回复
            </a>
        </div>
        <blockquote class="d-none"></blockquote>
    </div>
</div>