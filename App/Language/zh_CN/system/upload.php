<?php

$language["field_error"] = "无效的文件上传字段";
$language["upload_dir_error"] = "上传的目录无效";
$language["input_size_error"] = "无效的定义最大上传大小 ";
$language["file_not_selected"] = "没有选择文件";
$language["max_size"] = "文件大小超过最大文件限制";
$language["invalid_file_extension"] = "无效的文件扩展名 ";
$language["unknown_problem"] = "未知的问题";
$language['file_is_exists'] = "文件已经存在,请不要重复上传";