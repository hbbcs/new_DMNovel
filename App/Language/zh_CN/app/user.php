<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-12-1
 * Time: 上午8:39
 */

$language['username_required'] = '用户名必须填写';
$language['password_required'] = '密码必须填写';
$language['login_success'] = '登录成功';
$language['not_find_username'] = '未找到用户';
$language['password_is_error'] = '输入的密码错误';