<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-10-31
 * Time: 下午4:45
 */
$language['folderExist'] = '目录名已经存在';
$language['createFolderSuccess'] = '创建目录成功';
$language['doNotUploadAtRoot'] = '不能在根目录下上传文件';
$language['doNotModifyAtRoot'] = '不能修改根目录下的文件夹名称';
$language['modifyFolderNameSuccess'] = '成功修改文件夹名称';
$language['oldPassError'] = '输入的原密码错误';
$language['renewPassError'] = '两次输入的密码不相同';
$language['newPassSuccess'] = '成功修改了密码';
$language['inputPasswordError'] = '输入的密码错误';
$language['passwordSuccess'] = '输入的密码正确，正在进入';
$language['deleteFolderSuccess'] = '删除文件夹成功';