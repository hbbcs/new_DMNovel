<?php namespace App;

use App\Models\Users;
use System\Cache;
use System\Config;
use System\Crypt;
use System\Db;
use System\Language;
use System\Request;
use System\Session;
use System\View;

class App
{

    protected $session;
    protected $config;
    protected $cache;
    protected $request;
    protected $view;
    protected $language;
    protected $db;
    protected $crypt;
    protected $user;

    public function __construct()
    {
        $this->config = new Config();

        $this->errors = [];

        $this->cache = new Cache($this->config->cache);
        $this->request = new Request();

        $this->view = new View($this->config->view);

        $this->language = new Language($this->config->language);

        if ($this->config->db) {
            $this->config->load('Database');
            $this->db = new Db($this->config->mysql);
        }

        $this->session = new Session();
        $this->session->start();

        $this->crypt = new Crypt();

        $this->setting = new \App\Models\Setting();
        $this->view->homeTitle = $this->setting->getSetting('title') ?: $this->config->title;
        $this->view->metaDesc = $this->setting->getSetting('metaDesc') ?: '';

        $userModel = new Users();
        $this->user = $userModel->checkLogin();
        if ($this->user) {
            $this->view->user = $this->user;
        }
    }

    /**
     * @param string $contentPage 要显示的页面内容
     * @param string $header 头文件，可以是单页文件
     * @param bool $single 是否是单页，如果为true：将contentPage转成字符串，输入到头文件变量$content
     * @param string $footer 脚文件，可以为空
     */
    public function render($contentPage, $header = 'header', $single = false, $footer = null)
    {
        $this->view->title = $this->view->title ?: $this->config->title;
        $this->view->assets = $this->config->baseUrl . "/assets";

        $ajax = $this->request->isAJAX();
        if (!$ajax) {
            if (!$single) {
                $this->view->content = $this->view->display($contentPage)->toString();
                if ($header) {
                    $this->view->display($header);
                }

                if ($footer) {
                    $this->view->display($footer);
                }

                $this->view->render();
            } else {
                if ($header) {
                    $this->view->display($header);
                }
                $this->view->display($contentPage);
                if ($footer) {
                    $this->view->display($footer);
                }

                $this->view->render();
            }
        } else {
            $this->view->display($contentPage)
                ->render();
        }
        exit();
    }

    /**
     * return json string
     *
     * @param string|array $message
     * @param string $status
     */
    public function showJson($message = '', $status = 'true')
    {

        if ($status == 'true') {
            $array = $message;
        } else {
            ($status) ? $array['status'] = $status : null;
            $array['message'] = $message;
        }
        echo json_encode($array, JSON_UNESCAPED_UNICODE);
        exit();
    }
}