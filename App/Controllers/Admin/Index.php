<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-12-4
 * Time: 下午4:26
 */

namespace App\Controllers\Admin;

use App\App;

class Index extends App
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->user) {
            showError("用户尚未登录，请登录后重新打开。", '', '无权限访问');
        } else if ($this->user['group']['id']!=1) {
            showError("用户无权限访问");
        }
    }

    public function index()
    {
        $this->view->story = $this->db->countAll('story');
        $this->view->users = $this->db->countAll('users', ['deleted' => 0]);
        $sql="select
        sum(truncate(data_length/1024/1024, 2)) as volume
        from information_schema.tables
        where table_schema='novel';";                
        $volume = $this->db->query($sql)->first();
        $volume = $volume['volume'] > 1024 ? round($volume['volume']/1024) ."GB" : $volume['volume'] ."MB";
        $this->view->volume =$volume;
        $today = strtotime(date("Y-m-d",time()));        
        $statModel=new \App\Models\Stat();
        for ($i=6;$i>=0;$i--) {
            $day=date("Y-m-d",$today-24*60*60*$i);
            $days[]=$day;           
            $stat=$statModel->where("date",$day)->first();
            $visit[]=$stat['visit']?:0;
            $story[]=$stat['story']?:0;
            $download[]=$stat['download']?:0;
        }
        $this->view->chartLabel='"'.implode("\",\"",$days).'"';
        $this->view->chartVisit=implode(",",$visit);
        $this->view->chartStory=implode(",",$story);
        $this->view->download=implode(",",$download);
        $this->view->title = "概览";
        $this->view->desc = "概览网站内容";
        $this->render('admin/home', 'admin/header');
    }
}