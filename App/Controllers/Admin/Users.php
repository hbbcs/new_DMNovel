<?php namespace App\Controllers\Admin;

use \App\App;

class Users extends App
{

    private $userModel;
    public function __construct(...$params)
    {
        parent::__construct();
        if (!$this->user) {
            showError("用户尚未登录，请登录后重新打开。", '', '无权限访问');
        } else if ($this->user['group']['id']!=1) {
            showError("用户无权限访问");
        }
        $this->userModel = new \App\Models\Users();
    }

    public function index()
    {
        $this->view->title = "用户管理";
        $this->view->desc = "彻底删除用户必须在回收站中删除";
        $this->render('admin/users/list', 'default');
    }

    public function table()
    {
        $table = $this->request->get();
        $deleted = $this->request->get('deleted') ?: 0;

        if (isset($table['search']) && $table['search'] != "") {
            $this->userModel->like('users.username', $table['search']);
        }

        if (isset($table['sort'])) {
            $this->userModel->order($table['sort'], $table['order']);
        }

        if (isset($table['limit'])) {
            $this->userModel->limit($table['limit'], $table['offset']);
        }

        $usersList = $this->userModel->select('users.*,groups.name as groupName,zone.name as zoneName')
            ->join('groups', 'users.group=groups.id', 'left')
            ->join('zone', 'users.zone=zone.id', 'left')
            ->where('deleted', $deleted)
            ->datatable();

        echo $usersList;
    }

    public function add($id = '')
    {
        if ($id) {
            $this->view->editUser = $this->userModel->find($id);
        }
        $group = new \App\Models\Group();
        $this->view->groups = $group->findAll();
        $cities = new \App\Models\Zone();
        $this->view->zones = $cities->findAll();
        $this->render('admin/users/add');
    }

    public function save()
    {
        $id = $this->request->post('id') ?: 0;
        $password = $this->request->post('password');
        $user = [
            "id" => $id,
            "username" => $this->request->post('username'),
            "email" => $this->request->post('email'),
            "group" => $this->request->post('group_id'),
            "zone" => $this->request->post('zone_id'),
        ];
        if ($id) {
            if ($password) {
                $user['password'] = md5($password);
            }
        } else {
            $user['password'] = $password ? md5($password) : md5('123');
        }
        $this->userModel->save($user);
        $this->showJson('保存用户成功', 'Success');
    }

    public function delete($recover = false)
    {
        $id = $this->request->postGet('id');

        if (!$id) {
            $this->showJson('没有选择要删除的用户', 'Error');
        }

        if (!is_array($id)) {
            $ids[] = $id;
        } else {
            $ids = $id;
        }

        $deleted = $recover == 'true' ? 0 : 1;
        $deleted_message = $recover == 'true' ? '恢复成功' : '删除成功';
        $this->db->table('users')
            ->where('id', $ids, 'in')
            ->set('deleted', $deleted)
            ->update();

        $this->showJson($deleted_message, 'Success');
    }

}