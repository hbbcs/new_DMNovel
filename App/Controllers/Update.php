<?php namespace App\Controllers;

use \App\App;

class Update extends App
{

    public function __construct(...$params) {
        parent::__construct();       
    }

    public function index($id=null) {
        $storyModel=new \App\Models\Story();
        if ($id) {
            $stories=$storyModel->where("id",$id)->findAll();
        } else {
            $stories=$storyModel->where("type","cloud")->findAll();
        }
        header('X-Accel-Buffering: no');
        ini_set("output_buffering","on");
               
        foreach ($stories as $story) {
            $storyModel->createDB($story)->cloudChapterList($story,true,true);
        }
    }
}