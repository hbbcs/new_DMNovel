<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 17-11-27
 * Time: 上午11:27
 */

namespace App\Controllers;

use \App\App;
use \App\Models\Users;

class User extends App
{

    private $categoryModel;
    public function __construct()
    {
        parent::__construct();
        $this->language->load('app/user');
        $this->categoryModel = new \App\Models\Category();
    }

    public function index()
    {
        $this->view->categories = $this->categoryModel->where(['parent' => 0])->findAll();
        $this->view->viewTitle = "用户登录";
        $this->render('front/user/login', 'front/header');
    }

    public function login()
    {
        $username = $this->request->post('username');
        $password = $this->request->post('password');
        $remember = $this->request->post('remember');
        $expire = $remember ? 3600 * 24 * 365 : 7200;

        $user = new Users();
        $user->login($username, $password, $expire);
        if ($user->error) {
            $user->error = str_replace(' ', '_', strtolower($user->error));
            $this->view->categories = $this->categoryModel->where(['parent' => 0])->findAll();
            $this->view->error = $this->language->get($user->error);
            //$this->render('front/user/login', 'front/header');
            $this->showJson($this->view->error, 'Error');
        } else {
            //redirect("/");
            $this->showJson($this->language->login_success, 'Success');
        }
    }

    public function logout()
    {
        $user = new \App\Models\Users();
        $user->logout();
        redirect("/");
    }

    public function config()
    {
        $this->view->categories = $this->categoryModel->where(['parent' => 0])->findAll();
        $this->render('front/user/config', 'front/header');
    }

    public function profile($id = null)
    {
        $userModel = new Users();
        $user = ($id) ? $userModel->find($id) : $this->user;
        $this->view->cuser = $user;
        $this->view->categories = $this->categoryModel->where(['parent' => 0])->findAll();
        $this->render('front/user/profile', 'front/header');
    }

    public function password()
    {
        $oldpassword = $this->request->post('oldpassword');
        if ($oldpassword) {
            $newpassword = $this->request->post('newpassword');
            $repassword = $this->request->post("repassword");
            if ($this->user['password'] != $this->crypt->md5($oldpassword)) {
                $this->showJson("输入的原密码错误", "Error");
            } else if ($newpassword != $repassword) {
                $this->showJson("两次输入的新密码不一致", "Error");
            } else {
                $userModel = new Users();
                $user = $userModel->find($this->user['id']);
                $user['password'] = $this->crypt->md5($newpassword);
                $userModel->save($user);
                $this->showJson("密码修改成功", "Success");
            }
        } else {
            $this->render('front/user/password', 'front/header');
        }
    }

    public function register()
    {
        $username = $this->request->post('username');
        $email = $this->request->post('email');
        $password = $this->request->post('password');
        $repassword = $this->request->post('repassword');

        if (!$username) {
            $this->render('front/user/register', 'front/header');
        } else {
            $userModel=new \App\Models\Users();
            if (!$password || !$repassword || !$email) {
                $this->showJson("所有输入项必须填写","Error");
            }
            if ($userModel->findUser($username)) { 
                $this->showJson("用户名已存在","Error");
            } else if ($userModel->findUser($email,1)) {
                $this->showJson("邮箱已存在","Error");
            } else if ($password!=$repassword) {
                $this->showJson("两次输入的密码不相同","Error");
            } else {
                $user=[
                    "username"=>$username,
                    "password"=>$this->crypt->md5($password),
                    "mail" => $email,
                    "avatar"=>"default.jpg",
                    "group"=>4,
                    "deleted"=>0
                ];
                $userModel->save($user);
                $this->showJson("注册成功，正在登录...","Success");
            }
        }
    }
}