-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: novel
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `api`
--

DROP TABLE IF EXISTS `api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `search` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `info` varchar(255) NOT NULL,
  `chapterList` varchar(255) NOT NULL,
  `chapter` varchar(255) NOT NULL,
  `static` varchar(255) NOT NULL,
  `rank` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api`
--

LOCK TABLES `api` WRITE;
/*!40000 ALTER TABLE `api` DISABLE KEYS */;
INSERT INTO `api` VALUES (1,'追书神器','http://api.zhuishushenqi.com/book/fuzzy-search','http://api.zhuishushenqi.com/toc','http://api.zhuishushenqi.com/book/','http://api.zhuishushenqi.com/atoc/','http://chapter2.zhuishushenqi.com/chapter/','http://statics.zhuishushenqi.com','http://api.zhuishushenqi.com/ranking/','http://api.zhuishushenqi.com/book/by-categories');
/*!40000 ALTER TABLE `api` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent` int(10) NOT NULL,
  `icon` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'玄幻',0,'icon-xuanhuan'),(2,'仙侠',0,'icon-xianxia'),(3,'武侠',0,'icon-wuxia'),(4,'都市',0,'icon-dushi'),(5,'职场',0,'icon-zhichang'),(6,'历史',0,'icon-lishi'),(7,'奇幻',0,'icon-qihuan'),(8,'军事',0,'icon-junshi'),(14,'游戏',0,'icon-youxi'),(15,'竞技',0,'icon-jingji'),(16,'科幻',0,'icon-kehuan'),(17,'灵异',0,'icon-lingyi'),(19,'东方玄幻',1,''),(20,'异界大陆',1,''),(21,'异界争霸',1,''),(22,'远古神话',1,''),(23,'古典仙侠',2,''),(24,'幻想修仙',2,''),(25,'现代修仙',2,''),(26,'洪荒封神',2,''),(27,'传统武侠',3,''),(28,'新派武侠',3,''),(29,'国术武侠',3,''),(30,'都市生活',4,''),(31,'爱情婚姻',4,''),(32,'异术超能',4,''),(33,'恩怨情仇',4,''),(34,'青春校园',4,''),(35,'现实百态',4,''),(36,'娱乐明星',5,''),(37,'官场沉浮',5,''),(38,'商场职场',5,''),(39,'穿越历史',6,''),(40,'架空历史',6,''),(41,'历史传记',6,''),(42,'西方奇幻',7,''),(43,'领主贵族',7,''),(44,'亡灵异族',7,''),(45,'魔法校园',7,''),(46,'军事战争',8,NULL),(47,'战争幻想',8,NULL),(48,'谍战特工',8,NULL),(49,'军旅生涯',8,NULL),(50,'抗战烽火',8,NULL),(51,'游戏生涯',14,NULL),(52,'电子竞技',14,NULL),(53,'虚拟网游',14,NULL),(54,'游戏异界',14,NULL),(55,'体育竞技',15,NULL),(56,'篮球运动',15,NULL),(57,'足球运动',15,NULL),(58,'棋牌桌游',15,NULL),(59,'星际战争',16,NULL),(60,'时空穿梭',16,NULL),(61,'未来世界',16,NULL),(62,'古武机甲',16,NULL),(63,'超级科技',16,NULL),(64,'进化变异',16,NULL),(65,'末世危机',16,NULL),(66,'推理侦探',17,NULL),(67,'恐怖惊悚',17,NULL),(68,'悬疑探险',17,NULL),(69,'灵异奇谈',17,NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collect`
--

DROP TABLE IF EXISTS `collect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(255) NOT NULL,
  `site_url` varchar(255) NOT NULL,
  `book_url` varchar(255) NOT NULL,
  `book_title` varchar(255) NOT NULL,
  `book_author` varchar(255) NOT NULL,
  `book_desc` varchar(255) NOT NULL,
  `book_img` varchar(255) NOT NULL,
  `book_list` varchar(255) NOT NULL,
  `chapter_list` varchar(255) NOT NULL,
  `chapter_url` varchar(255) NOT NULL,
  `chapter_content` varchar(255) NOT NULL,
  `test_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collect`
--

LOCK TABLES `collect` WRITE;
/*!40000 ALTER TABLE `collect` DISABLE KEYS */;
INSERT INTO `collect` VALUES (1,'顶点小说','http://www.23us.com/','http://www.23us.com/book/:book_id','#content dd:eq(0) h1','#content dd:eq(1) table td:eq(1)','#content dd:eq(3) p:eq(1)','#content dd:eq(1) div:eq(0) img','#content dd:eq(1) .btnlinks a:eq(0)','.bdsub table td a',':book_url/','#contents ',55519),(2,'笔趣阁','http://www.qu.la/','http://www.qu.la//book/:book_id','#info h1','#info p:eq(0)','#intro p:eq(0)','#fmimg img','http://www.qu.la/book/:book_id','#list dd a',':book_url/','#content',14),(3,'三五文学网','http://www.555zw.com','http://www.555zw.com/bookinfo/:book_id[2]/:book_id.htm','span#title a','div#title span:eq(1) a','.rightDiv div:eq(6)','.picborder','span#title a','.acss td a',':book_url/','#content',31281),(4,'看书网','http://www.kanshu.com/','http://www.kanshu.com/artinfo/:book_id.html','.title_h1 .div1 h1','.title_h1 .div2 span:eq(1) a','#articledesc','.xx_left1 img','http://www.kanshu.com/files/article/html/:book_id','.mulu_list li a',':site_url','div.yd_text2',28942),(5,'2018小说网','http://ww018pc.com/','http://www.2018pc.com/qidian/xiaoshuo/:book_id.html','#content h1 a','.novel_msg li:eq(0) a','#description1','.novel_img a img','.button2 a','.novel_list ul li a',':book_url','.novel_content',10364);
/*!40000 ALTER TABLE `collect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collect_cache`
--

DROP TABLE IF EXISTS `collect_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collect_cache` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `collect_id` int(10) NOT NULL,
  `book_id` varchar(255) NOT NULL,
  `list_url` varchar(255) NOT NULL,
  `chapter_url` varchar(255) NOT NULL,
  `story_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL,
  `update_time` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collect_cache`
--

LOCK TABLES `collect_cache` WRITE;
/*!40000 ALTER TABLE `collect_cache` DISABLE KEYS */;
INSERT INTO `collect_cache` VALUES (1,'大主宰 ',1,'28373','http://www.23us.com/html/28/28373/','http://www.23us.com/html/28/28373//',1,1,'2017-06-01');
/*!40000 ALTER TABLE `collect_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `story` varchar(64) NOT NULL,
  `user` int(11) NOT NULL,
  `content` varchar(100) NOT NULL,
  `date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `story` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'超级管理员'),(2,'管理员'),(3,'作者'),(4,'读者');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rankApi`
--

DROP TABLE IF EXISTS `rankApi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rankApi` (
  `id` varchar(32) NOT NULL,
  `title` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rankApi`
--

LOCK TABLES `rankApi` WRITE;
/*!40000 ALTER TABLE `rankApi` DISABLE KEYS */;
INSERT INTO `rankApi` VALUES ('548e40f2c58cff632353e730','读者留存率'),('548e97e29fb698a01dc6ee6f','追书最热榜'),('548e984e5beb6f0458d652aa','追书完结榜'),('548e9883c58cff632353e731','起点月票榜'),('548e98c55beb6f0458d652ab','纵横月票榜'),('548e9915fb8190536fb097d0','17K 鲜花榜'),('548e99423b5a77135a798b58','创世月票榜'),('549d1ab5796b680a50686f47','晋江月票榜'),('549d1ad11cd9d5d26ee1b6c2','潇湘月票榜');
/*!40000 ALTER TABLE `rankApi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search`
--

DROP TABLE IF EXISTS `search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search`
--

LOCK TABLES `search` WRITE;
/*!40000 ALTER TABLE `search` DISABLE KEYS */;
INSERT INTO `search` VALUES (1,'天',279),(2,'天啊',53),(3,'天阿降临',71),(4,'林川',29),(5,'天蚕土豆',67),(6,'烟雨江南',24),(7,'古龙',18),(8,'隋唐',26),(9,'隋唐大猛士',2),(10,'死亡因子',2),(11,'晚餐谋杀案',5),(12,'贩罪',2),(13,'三天',2),(14,'范青',3),(15,'天阿',6),(16,'木子蓝色',3),(17,'剑来',2);
/*!40000 ALTER TABLE `search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `desc` mediumtext NOT NULL,
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES (1,'title','主页标题','東木书屋'),(2,'chapter_cache_time','章节缓存时间','30000'),(3,'content_filter','过滤内容敏感字','{\"<a href=\\\"http:\\/\\/www.01bz.in\\/.+\\\"><u>%%<\\/u><\\/a>\":\"\\\\1\"}'),(4,'per_page','分类中文章列表数量','20'),(5,'apiSource','追书API默认使用源','my176');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `story`
--

DROP TABLE IF EXISTS `story`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `story` (
   `id` varchar(64) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `desc` mediumtext NOT NULL,
  `vote` smallint(6) NOT NULL DEFAULT '0',
  `score` smallint(6) NOT NULL DEFAULT '0',
  `average` decimal(2,1) NOT NULL DEFAULT '0.0',
  `favorites` mediumint(9) NOT NULL DEFAULT '0' COMMENT '收藏数',
  `click` int(11) NOT NULL DEFAULT '0',
  `category` varchar(10) DEFAULT NULL,
  `sub_category` varchar(11) DEFAULT NULL,
  `time` int(11) NOT NULL,
  `last_update` int(11) DEFAULT NULL,
  `last_chapter` varchar(255) DEFAULT NULL,
  `last_chapter_id` int(11) DEFAULT NULL,
  `cover` varchar(255) NOT NULL,
  `cloud` varchar(255) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `type` varchar(5) NOT NULL DEFAULT 'local',
  `approve` tinyint(1) NOT NULL DEFAULT '0'
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `story`
--

LOCK TABLES `story` WRITE;
/*!40000 ALTER TABLE `story` DISABLE KEYS */;
/*!40000 ALTER TABLE `story` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` char(20) NOT NULL,
  `password` char(32) NOT NULL,
  `mail` varchar(200) DEFAULT NULL,
  `avatar` char(255) DEFAULT NULL,
  `group` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','a6ef37564b0e65dae4da498cada6e090','','',1,0,0),(2,'guest','202cb962ac59075b964b07152d234b70','guest@hbbcs.com',NULL,4,0,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `votes`
--

DROP TABLE IF EXISTS `votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `num` tinyint(2) NOT NULL,
  `reset` int(11) NOT NULL COMMENT '重置时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `votes`
--

LOCK TABLES `votes` WRITE;
/*!40000 ALTER TABLE `votes` DISABLE KEYS */;
INSERT INTO `votes` VALUES (1,1,0,1557284703);
/*!40000 ALTER TABLE `votes` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE IF NOT EXISTS `notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(200) NOT NULL,
  `user` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `visit` int(11) NOT NULL,
  `story` int(11) NULL,
  `download` int(11) NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`date`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;